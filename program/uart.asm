;
; UART_init
;
;  Sets up UART for 576200 baud, 8bits, no parity, 1 stop bit.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
UART_init:
	; Set 57600 baud.
	eor r16, r16
	sts UBRR0H, r16
	ldi r16, 16 ; 0x67 for 9600 baud
	sts UBRR0L, r16
	; Set 8bit no parity.
	ldi r16, 0x03 << UCSZ00
	sts UCSR0C, r16
	; Enable RX and TX.
	lds r16, UCSR0B
	ori r16, (1 << RXEN0) | (1 << TXEN0)
	sts UCSR0B, r16
	; The end.
	ret

;
; UART_sendbyte
;
;  Sends a byte via UART.
;
;  Input:
;    r16: The byte.
;
;  Output:
;    -
;
;  Side effects:
;    UART output lol, and it can take time since this is a blocking function.
;
UART_sendbyte:
	push r17
UART_sendbyte_blk:
	lds r17, UCSR0A
	andi r17, 1 << UDRE0
	breq UART_sendbyte_blk
	pop r17
	sts UDR0, r16
	ret

;
; UART_recvbyte
;
;  Receive a byte via UART.
;
;  Input:
;    -
;
;  Output:
;    r16: The byte.
;
;  Side effects:
;    This is a blocking function.
;
UART_recvbyte:
	lds r16, UCSR0A
	andi r16, 1 << RXC0
	breq UART_recvbyte
	lds r16, UDR0
	ret

;
; UART_sendstring
;
;  Send a string from program memory to UART.
;
;  Input:
;    Z: Point to a 0 terminated string.
;
;  Output:
;    -
;
;  Side effects:
;    This is a blocking function.
;    Z, r16 and r17 trashed.
;
UART_sendstring:
	lpm r16, Z+
	andi r16, 0xFF
	breq UART_sendstring_end
UART_sendstring_block:
	lds r17, UCSR0A
	andi r17, 1 << UDRE0
	breq UART_sendstring_block
	sts UDR0, r16
	rjmp UART_sendstring
UART_sendstring_end:
	ret

;
; UART_sendramstring
;
;  Send a string from data memory to UART.
;
;  Input:
;    Z: Point to a 0 terminated string.
;
;  Output:
;    -
;
;  Side effects:
;    This is a blocking function.
;    Z, r16 and r17 trashed.
;
UART_sendramstring:
	ld r16, Z+
	andi r16, 0xFF
	breq UART_sendramstring_end
UART_sendramstring_block:
	lds r17, UCSR0A
	andi r17, 1 << UDRE0
	breq UART_sendramstring_block
	sts UDR0, r16
	rjmp UART_sendramstring
UART_sendramstring_end:
	ret
