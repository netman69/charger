/******************
 * Generic things *
 ******************/

function $(id) {
	return document.getElementById(id);
}

function getFile(url, callback) {
	var xhr = new XMLHttpRequest();
	if ("onload" in xhr) {
		/* onreadystatechange does not work in FF2 */
		xhr.onload = function () {
			callback(200, this.responseText);
		};
		xhr.onerror = function () {
			callback(0, this.responseText);
		};
	} else {
		xhr.onreadystatechange = function () {
			if (this.readyState == 4)
				callback(this.status, this.responseText);
		};
	}
	xhr.open("GET", url, true);
	if (xhr.overrideMimeType != undefined)
		xhr.overrideMimeType('text/plain');
	try {
		xhr.send(null); /* Old fire foxes (2.0, 3.0) need the null argument */
	} catch (e) {
		callback(null, e.message);
	}
}

/******************
 * Page switching *
 ******************/

var pages = [
	{ name: "status", el: $("status"), btn: $("btn_status"), callback: null },
	{ name: "cells", el: $("cells"), btn: $("btn_cells"), callback: null },
	{ name: "cell", el: $("cell"), btn: null, callback: loadCell },
	{ name: "settings", el: $("settings"), btn: $("btn_settings"), callback: null }
];
var pages_default = "status"; /* Fallback in case no valid hash */
var pages_active = null;
var hash = []; /* This holds everything after "#" split by "=" */

function hashChange() {
	hash = window.location.hash.substring(1).split("=");
	/* If no hash go to default page */
	if (hash[0].length == 0)
		hash = [ pages_default ];
	/* If a page is active, hide it and deselect button. */
	if (pages_active != null) {
		pages_active.el.style.display = "none";
		if (pages_active.btn != null)
			pages_active.btn.style["text-decoration"] = "none";
	}
	/* Find new active page */
	pages_active = null;
	pages.forEach(function(page) {
		if (page.name == hash[0])
			pages_active = page;
	});
	/* If we found it show page, select button and run callback */
	if (pages_active != null) {
		pages_active.el.style.display = "block";
		if (pages_active.btn != null)
			pages_active.btn.style["text-decoration"] = "underline";
		if (pages_active.callback != null)
			pages_active.callback();
	} else {
		/* If not go to default page */
		window.location.hash = "";
		hashChange();
	}
}

window.onhashchange = hashChange;
hashChange();

/******************
 * Status updates *
 ******************/

function status_update() {
	getFile("status.txt", function(status, data) {
		var lines = data.split(/[\r\n]+/);
		$("statusTime").innerHTML = lines[0];
		status_fill($("cha"), lines[1]);
		status_fill($("chb"), lines[2]);
	});
	/* Schedule next run */
	setTimeout(status_update, 1000);
}

function status_fill(tab, line) {
	var status = line.split(";");
	/* Cell ID */
	tab.getElementsByClassName("statusCell")[0].innerHTML = status[0];
	if (status[4] == "-")
		tab.getElementsByClassName("statusCell")[0].href = "";
	else tab.getElementsByClassName("statusCell")[0].href = "#cell=" + status[0];
	/* State */
	if (status[1] == "O") {
		tab.getElementsByClassName("statusState")[0].innerHTML = "Off";
		tab.getElementsByClassName("statusState")[0].style.background = "silver";
	}
	if (status[1] == "C") {
		tab.getElementsByClassName("statusState")[0].innerHTML = "Charging";
		tab.getElementsByClassName("statusState")[0].style.background = "orange";
	}
	if (status[1] == "D") {
		tab.getElementsByClassName("statusState")[0].innerHTML = "Done";
		tab.getElementsByClassName("statusState")[0].style.background = "lime";
	}
	/* Set voltage and current */
	tab.getElementsByClassName("statusVSet")[0].innerHTML = status[2] / 1000 + "V";
	tab.getElementsByClassName("statusISet")[0].innerHTML = status[3] + "mA";
	/* Measured voltage and current */
	if (status[4] == "-")
		tab.getElementsByClassName("statusVMes")[0].innerHTML = "-";
	else tab.getElementsByClassName("statusVMes")[0].innerHTML = status[4] / 1000 + "V";
	if (status[5] == "-")
		tab.getElementsByClassName("statusIMes")[0].innerHTML = "-";
	else tab.getElementsByClassName("statusIMes")[0].innerHTML = status[5] + "mA";
}

status_update(); /* Function will automatically schedule re-run every second. */

/******************
 * Cell info page *
 ******************/

function getRow(head, row) {
	var ret = {};
	var vals = row.split(";");
	for (var i = 0; i < vals.length; ++ i)
		ret[head[i]] = vals[i];
	return ret;
}

function loadCell() {
	$("cellData").style.display = "none";
	$("cellError").style.display = "none";
	if (hash.length < 2 || hash[1].length == 0) {
		loadCellError("Invalid hash.");
		return;
	}

	var filename = "cell" + hash[1] + ".csv";	
	var table = $("cellTable");
	table.innerHTML = "<tr><th>Time</th><th>Voltage</th><th>Current</th><th>Capacity</th></tr>";
	$("cellDownload").href = filename;
	$("cellDownloadGraph").download = filename.replace(/\.csv$/, ".png"); /* graph_draw() will update href */
	$("cellID").innerHTML = "#" + hash[1];

	/* TODO Multiple charge cycles
	 *   Each cell should have a folder, and folder listings able to download.
	 */
	getFile(filename, function(status, data) {
		if (status != 200) {
			loadCellError((status == null) ? data : ("Error " + status));
			return;
		}

		$("cellData").style.display = "block";
		
		var rows = []; /* the table after parsing, for graphing etc */
		var lines = data.split(/[\r\n]+/);
		var head = lines[0].split(";");
		
		for (var i = 1; i < lines.length; ++i) {
			if (lines[i].trim().length == 0)
				continue;
			var cols = getRow(head, lines[i]);
			var row = table.insertRow(-1);
			cols["voltage"] = parseInt(cols["voltage"]);
			cols["current"] = parseInt(cols["current"]);
			cols["capacity"] = parseInt(cols["capacity"]);
			rows[i - 1] = cols;
			row.insertCell(-1).innerHTML = cols["time"];
			row.insertCell(-1).innerHTML = cols["voltage"];
			row.insertCell(-1).innerHTML = cols["current"];
			row.insertCell(-1).innerHTML = cols["capacity"];
		}
		
		graph_draw(rows);
	});
}

function loadCellError(msg) {
	$("cellData").style.display = "none";
	$("cellError").style.display = "block";
	$("cellErrorMessage").innerHTML = msg;
}

/*******************
 * Cell info graph *
 *******************/

function graph_draw(rows) {
	var c = $("cellCanvas");
	graph_draw_int(rows, c, "rgba(0, 0, 0, 0)", "midnightblue", "#FFFFFF", "#CCCCCC");
	/* Make the download link work */
	/* We make a hidden canvas so we can make different colors */
	var hc = document.createElement('canvas');
	hc.width = c.width;
	hc.height = c.height;
	graph_draw_int(rows, hc, "rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0)", "#444444", "#666666");
	$("cellDownloadGraph").href = hc.toDataURL();
}

function graph_draw_int(rows, c, bg, bg_grat, fg_text, fg_grat) {
	var ctx = c.getContext("2d");
	var w = c.width;
	var h = c.height;
	
	var color_v = "lime";
	var color_i = "red";
	var color_c = "orange";
	
	var margin_l = 60;
	var margin_r = 130;
	var margin_t = 20;
	var margin_b = 45;
	
	var l = margin_l;
	var t = margin_t;
	var r = w - margin_r;
	var b = h - margin_b;
	
	/* Clear the canvas and reset any transforms */
	ctx.setTransform(1, 0, 0, 1, 0, 0);
	ctx.clearRect(0, 0, w, h);
	ctx.lineWidth = 1;
	
	/* Fill the background */
	ctx.beginPath();
	ctx.rect(0, 0, w, h);
	ctx.fillStyle = bg;
	ctx.fill();
	
	ctx.beginPath();
	ctx.rect(l, t, r - l, b - t);
	ctx.fillStyle = bg_grat;
	ctx.fill();

	/* Translate the whole context half a pixel */
	/* This makes 1px straight lines show up as actual 1px */
	ctx.translate(0.5, 0.5);

	/*******************************/
	/* Prepare variables for graph */
	/*******************************/
	
	/* Determine min and max values */
	var max_v = 0, max_i = 0, max_c = 0;
	var min_v = 100000, min_i = 100000, min_c = 100000;
	for (var i = 0; i < rows.length; ++i) {
		if (rows[i]["voltage"] > max_v)
			max_v = rows[i]["voltage"];
		if (rows[i]["current"] > max_i)
			max_i = rows[i]["current"];
		if (rows[i]["capacity"] > max_c)
			max_c = rows[i]["capacity"];
		if (rows[i]["voltage"] < min_v)
			min_v = rows[i]["voltage"];
		if (rows[i]["current"] < min_i)
			min_i = rows[i]["current"];
		if (rows[i]["capacity"] < min_c)
			min_c = rows[i]["capacity"];
	}

	// TODO Calculate time, in case a second is skipped?
	
	/* Set number of vertical and horizontal divisions */
	var ndiv_v = 6;
	var ndiv_h = 7;
	
	/**********************/
	/* Draw the graticule */
	/**********************/

	ctx.fillStyle = fg_text;
	ctx.strokeStyle = fg_grat;
	
	// TODO Mark state changes (charging, done, etc).
	
	/* Draw rectangle to contain it */
	ctx.beginPath();
	ctx.moveTo(l, t);
	ctx.lineTo(l, b);
	ctx.lineTo(r, b);
	ctx.lineTo(r, t);
	ctx.lineTo(l, t);
	ctx.stroke();

	/* Draw vertical divisions */
	var div_v = (b - t) / ndiv_v;
	ctx.beginPath();
	for (var i = 1; i < ndiv_v; ++i) {
		ctx.moveTo(l, Math.round(b - div_v * i));
		ctx.lineTo(r, Math.round(b - div_v * i));
	}
	ctx.stroke();
	
	/* Extra vertical division for 3d column */
	ctx.beginPath();
	for (var i = 0; i < ndiv_v + 1; ++i) {
		ctx.moveTo(r + 55, Math.round(b - div_v * i));
		ctx.lineTo(r + 65, Math.round(b - div_v * i));
	}
	ctx.moveTo(r + 65, t);
	ctx.lineTo(r + 65, b);
	ctx.stroke();

	/* Draw horizontal divisions */
	var div_h = (r - l) / ndiv_h;
	ctx.beginPath();
	for (var i = 1; i < ndiv_h; ++i) {
		ctx.moveTo(Math.round(l + div_h * i), t);
		ctx.lineTo(Math.round(l + div_h * i), b);
	}
	ctx.stroke();
	
	/* Draw text for the vertical divisions */
	ctx.textBaseline = "middle";
	ctx.textAlign = "right";
	for (var i = 0; i < ndiv_v + 1; ++i)
		ctx.fillText(Math.round(min_v + i * ((max_v - min_v) / ndiv_v)) + "mV", l - 5, b - div_v * i);
	ctx.textAlign = "left";
	for (var i = 0; i < ndiv_v + 1; ++i)
		ctx.fillText(Math.round(min_i + i * ((max_i - min_i) / ndiv_v)) + "mA", r + 5, b - div_v * i);
	for (var i = 0; i < ndiv_v + 1; ++i)
		ctx.fillText(Math.round(min_c + i * ((max_c - min_c) / ndiv_v)) + "mAh", r + 70, b - div_v * i);
	
	/* Draw text for the time */
	ctx.textBaseline = "top";
	ctx.textAlign = "center";
	for (var i = 0; i < ndiv_h + 1; ++i)
		ctx.fillText(Math.round(i * ((rows.length - 1) / ndiv_h)) + "s", l + div_h * i, b + 5);
	
	/******************/
	/* Draw a legend  */
	/******************/
	
	var legend_ll = 20; // Line length
	var legend_offset = l + 72;
	var legend_voffset = b + 30;
	var legend_itemwidth = 100;
	var legend_textoffset = legend_ll + 5;
	
	ctx.lineWidth = 2;
	ctx.fillStyle = fg_text;
	ctx.textBaseline = "middle";
	ctx.textAlign = "left";
	
	/* Draw the lines */
	ctx.strokeStyle = color_v;
	ctx.beginPath();
	ctx.moveTo(legend_offset, legend_voffset);
	ctx.lineTo(legend_offset + legend_ll, legend_voffset);
	ctx.stroke();
	ctx.fillText("Voltage", legend_offset + legend_textoffset, legend_voffset);
	
	legend_offset += legend_itemwidth;
	ctx.strokeStyle = color_i;
	ctx.beginPath();
	ctx.moveTo(legend_offset, legend_voffset);
	ctx.lineTo(legend_offset + legend_ll, legend_voffset);
	ctx.stroke();
	ctx.fillText("Current", legend_offset + legend_textoffset, legend_voffset);
	
	legend_offset += legend_itemwidth;
	ctx.strokeStyle = color_c;
	ctx.beginPath();
	ctx.moveTo(legend_offset, legend_voffset);
	ctx.lineTo(legend_offset + legend_ll, legend_voffset);
	ctx.stroke();
	ctx.fillText("Capacity", legend_offset + legend_textoffset, legend_voffset);
	
	/******************/
	/* Graph the data */
	/******************/
	
	ctx.lineWidth = 2;
	ctx.strokeStyle = color_v;
	graph_draw_int_col(ctx, "voltage", -min_v, (b - t) / (max_v - min_v));
	ctx.strokeStyle = color_i;
	graph_draw_int_col(ctx, "current", -min_i, (b - t) / (max_i - min_i));
	ctx.strokeStyle = color_c;
	graph_draw_int_col(ctx, "capacity", -min_c, (b - t) / (max_c - min_c));

	function graph_draw_int_col(ctx, col, offset, slope) { /* Graph one column */
		ctx.beginPath();
		var hs = (r - l) / (rows.length - 1);
		ctx.moveTo(l, b - ((offset + rows[0][col]) * slope));
		for (i = 1; i < rows.length; ++i) {
			ctx.lineTo(l + i * hs, b - ((offset + rows[i][col]) * slope));
		}
		ctx.stroke();	
	}
}
