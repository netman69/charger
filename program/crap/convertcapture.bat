@echo off
rem convert raw bitmap file with imagemagick
convert -contrast-stretch 0 -rotate 90 -depth 8 -size 480x256+0 gray:capture.txt image.png
convert image.png -resize 550x480! image_scaled.png
pause
