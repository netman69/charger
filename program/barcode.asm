;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Barcode stuff.                          ;;
;;                                         ;;
;; This file needs camera.asm to assemble. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; barcode_getpix
;
;  Waits for the next line, get 8 pixels at specified horizontal offset.
;
;  Input:
;    r18: Horizontal offset.
;
;  Output:
;    r16: Pixel values. LSB is left, MSB is right.
;
;  Side effects:
;    r17 trashed.
;
barcode_getpix:
.macro pixdelay
	ldi r18, 5
	;ldi r18, 3
	dec r18
	brne -2
.endm
	call sync_wait
	; Wait a little bit.
	push r18
	dec r18
	brne -2
	; Read a few pixels.

	;in r16, PORTB
	;com r16
	;out PORTB, r16
	
	clr r16
	sbic v_dport, v_dport_data
	sbr r16, 1 << 0
	pixdelay
	sbic v_dport, v_dport_data
	sbr r16, 1 << 1
	pixdelay
	sbic v_dport, v_dport_data
	sbr r16, 1 << 2
	pixdelay
	sbic v_dport, v_dport_data
	sbr r16, 1 << 3
	pixdelay
	sbic v_dport, v_dport_data
	sbr r16, 1 << 4
	pixdelay
	sbic v_dport, v_dport_data
	sbr r16, 1 << 5
	pixdelay
	sbic v_dport, v_dport_data
	sbr r16, 1 << 6
	pixdelay
	sbic v_dport, v_dport_data
	sbr r16, 1 << 7
	pop r18
	
	;in r16, PORTB
	;com r16
	;out PORTB, r16
	
	ret

;
; barcode_readline
;
;  Read a vertical line from the camera.
;
;  Input:
;    r18: Horizontal offset.
;
;  Output:
;    SRAM (VIDEO_BUF)
;
;  Side effects:
;    r16, r17, r18, r19, r30(ZL), r31(ZH) trashed.
;
barcode_readline:
	call vsync_wait
	clr r17
	cpse r20, r17
	rjmp barcode_readline
	; Set Z to VIDEO_BUF.
	ldi r31, high(VIDEO_BUF)
	ldi r30, low(VIDEO_BUF)
	call barcode_readlinex
	; Second field.
barcode_readlineb:
	call vsync_wait
	ldi r17, 1
	cpse r20, r17
	rjmp barcode_readlineb
	; Set Z to 0x101.
	ldi r31, high(VIDEO_BUF)
	ldi r30, low(VIDEO_BUF) + 1
	call barcode_readlinex
	ret
barcode_readlinex:
	ldi r19, v_lines / 2
barcode_readlinex_pix:
	call barcode_getpix
	st Z+, r16
	adiw r31:r30, 1
	dec r19
	brne barcode_readlinex_pix
	ret

;
; barcode_read
;
;  Attempt to read a barcode.
;
;  Input:
;    r18: Horizontal offset.
;
;  Output:
;    r23: Code value.
;    Z flag: Set on success.
;
;  Side effects:
;    r16, r17, r19, r20, r21, r22, r23, r24, r25, r26 (XL), r30(ZL), r31(ZH) trashed.
;    SRAM 0x100-0x2ff trashed.
;
barcode_read:
	call barcode_readline
	ldi r25, 1
	call barcode_read_tryline
	breq barcode_read_success
	lsl r25
	call barcode_read_tryline
	breq barcode_read_success
	lsl r25
	call barcode_read_tryline
	breq barcode_read_success
	lsl r25
	call barcode_read_tryline
	breq barcode_read_success
	lsl r25
	call barcode_read_tryline
	breq barcode_read_success
	lsl r25
	call barcode_read_tryline
	breq barcode_read_success
	lsl r25
	call barcode_read_tryline
	breq barcode_read_success
	lsl r25
	call barcode_read_tryline
barcode_read_success:
	ret
barcode_read_tryline:
	; Set Z to 0x100, that's where our line is at.
	ldi r31, high(VIDEO_BUF)
	ldi r30, low(VIDEO_BUF)
	; Initialize registers we're going to use.
	clr r26 ; Bit counter.
	clr r21 ; Window.
	clr r22 ; Last bit for hysteresis on the window filter.
	clr r23 ; Here we shift in the bits that were decoded.
	clr r24 ; Even through to here.
	; Shift some bits into the window.
barcode_read_prime:
	; Load a bit from the memory.
	ld r16, Z+
	; Get the requested bit.
	clr r17
	and r16, r25
	breq +1
	inc r17
	; Shift that shit into our window.
	lsr r17
	rol r21
	cpi r30, low(VIDEO_BUF) + 4
	brne barcode_read_prime
barcode_read_decodebit_next: ; Decode (another) bit.
	clr r19 ; This holds the last seen pixels so we can detect W-B transition.
	clr r20 ; B/W "ratio".
barcode_read_decodebit:
	; Load a bit from the memory.
	ld r16, Z+
	clr r17 ; This part separates the bit we wanna look at.
	and r16, r25
	breq +1
	inc r17
	; Shift that shit into our window.
	lsr r17
	rol r21
	andi r21, 0b11111
	cpi r21, 0b11111
	brne +1
	ldi r22, 1
	cpi r21, 0
	brne +1
	ldi r22, 0
	mov r16, r22
	; Black pixels count r20 up, white ones count it down.
	sbrs r16, 0
	inc r20
	sbrc r16, 0
	dec r20
	; check 
	lsr r16
	rol r19
	andi r19, 0b11
	cpi r19, 0b10
	breq barcode_read_step ; We decoded a bit, go to process it.
barcode_read_next:
	cpi r31, high(VIDEO_BUF) ; Count until ZH overflows.
	breq barcode_read_decodebit
	cpi r30, v_lines - 254 ; Count until ZL is where we want it.
	brne barcode_read_decodebit
	; We reach here on invalid barcode.
	clz
	ret
barcode_read_step: ; This runs each time a bit is "decoded", and checks if we have a valid barcode.
	; Collect bits and check checksum.
	cpi r20, 128 ; Carry set if r20 > 128 (More white than black, or a stupid amount of bits).
	rol r23 ; low byte of code
	rol r24 ; high byte of code
	; Skip checksum if not enough bits.
	inc r26
	cpi r26, 14
	brcc +1
	rjmp barcode_read_decodebit_next
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Calculate and verify checksum ;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	; We hava r16, r17, r19 to work with
	clr r19
	; parity for bits 0, 1
	mov r16, r23
	mov r17, r23
	lsr r17
	add r17, r16
	sbrc r17, 0 ; even
	sbr r19, 1 << 2
	; parity for bits 2, 3
	mov r16, r23
	lsr r16
	lsr r16
	mov r17, r23
	lsr r17
	lsr r17
	lsr r17
	add r17, r16
	sbrs r17, 0 ; odd
	sbr r19, 1 << 3
	; parity for bits 4, 5
	mov r16, r23
	swap r16
	mov r17, r23
	swap r17
	lsr r17
	add r17, r16
	sbrc r17, 0 ; even
	sbr r19, 1 << 4
	; parity for bits 6, 7
	mov r16, r23
	swap r16
	lsr r16
	lsr r16
	mov r17, r23
	swap r17
	lsr r17
	lsr r17
	lsr r17
	add r17, r16
	sbrs r17, 0 ; odd
	sbr r19, 1 << 5
	; parity for bits 0, 3, 4, 7
	mov r16, r23 ; 0 already there
	mov r17, r23
	lsr r17
	lsr r17
	lsr r17
	add r16, r17 ; 3
	lsr r17
	add r16, r17 ; 4
	lsr r17
	lsr r17
	lsr r17
	add r16, r17 ; 7
	sbrs r16, 0 ; odd
	sbr r19, 1 << 1
	; parity for bits 1, 2, 5, 6
	mov r16, r23
	lsr r16 ; 1
	mov r17, r23
	lsr r17
	lsr r17
	add r16, r17 ; 2
	lsr r17
	lsr r17
	lsr r17
	add r16, r17 ; 5
	lsr r17
	add r16, r17 ; 6
	sbrc r16, 0 ; even
	sbr r19, 1 << 0
	; Checksum is now in r19
	cp r24, r19
	breq +1 ; barcode_read_decodebit_next too far to jump with conditional.
	rjmp barcode_read_decodebit_next
	;;;;;;;;;;;;;;;;;
	;; Valid code! ;;
	;;;;;;;;;;;;;;;;;
	sez
	ret
