;
; SPI_init
;
;  Initialize SPI for 5MHz master mode.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
SPI_init:
	; Set speed.
	ldi r16, (1 << SPE) | (1 << MSTR) ; Fosc/4, 5MHz.
	out SPCR, r16
	clr r16
	out SPSR, r16
	; Set MOSI and SCK output, leave others alone.
	in r16, DDRB
	ori	r16, (1 << DDB3) | (1 << DDB5)
	out DDRB, r16
	ret

;
; SPI_send
;
;  Send a byte via SPI (blocking).
;
;  Input:
;    r16
;
;  Output:
;    -
;
;  Side effects:
;    -
;
SPI_send:
	out SPDR, r16
	push r20
SPI_send_block:
	in r20, SPSR
	sbrs r20, SPIF
	rjmp SPI_send_block
	pop r20
	ret

;
; SPI_recv
;
;  Receive a byte via SPI (blocking).
;
;  Input:
;    -
;
;  Output:
;    r16
;
;  Side effects:
;    -
;
SPI_recv:
	ldi r16, 0xFF
	call SPI_send
	in r16, SPDR
	ret
