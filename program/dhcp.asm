DHCP_renew:
	; We'll use socket 3 for all.
	ldi r28, 3
	; Set UDP mode.
	ldi r19, 0
	ldi r18, 68
	call WNet_udpopen
	; Destination IP = 255.255.255.255
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_DIPR0
	ldi r17, 0xFF
	call WNet_write
	adiw r25:r24, 1
	call WNet_write
	adiw r25:r24, 1
	call WNet_write
	adiw r25:r24, 1
	call WNet_write
	; Destination port = 67.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_DPORT0
	ldi r17, 0x00
	call WNet_write
	adiw r25:r24, 1
	ldi r17, 67
	call WNet_write
	; Send DHCPDISCOVER packet.
	ldi r31, high(WNet_DHCPC_msg_discover << 1)
	ldi r30, low(WNet_DHCPC_msg_discover << 1)
	ldi r19, high(300)
	ldi r18, low(300)
	call WNet_send_pmem
	
	; Wait for send complete.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
DHCP_renew_waitsend:
	call WNet_read
	cpi r16, 0
	brne DHCP_renew_waitsend
.if 1
	; Wait for response. TODO
	ldi r31, high(TMP_BUF)
	ldi r30, low(TMP_BUF)
	ldi r19, high(64)
	ldi r18, low(64)
	call WNet_recv

	; Send it to serial. TODO
	ldi r31, high(TMP_BUF)
	ldi r30, low(TMP_BUF)
	ldi r20, 64
wnet_test_uloop2:
	ld r16, Z+
	call UART_sendbyte
	dec r20
	brne wnet_test_uloop2
.endif
	ret

WNet_DHCPC_msg_discover:
.db 0x01, 0x01, 0x06, 0x00 ; OP, HTYPE, HLEN, HOPS
.db 0x02, 0x23, 0x45, 0x67 ; XID TODO randomize/sequence
.db 0x00, 0x00, 0x00, 0x00 ; SECS, FLAGS ; TODO find out what flags unicast/multicast shit is
.db 0x00, 0x00, 0x00, 0x00 ; CIADDR
.db 0x00, 0x00, 0x00, 0x00 ; YIADDR
.db 0x00, 0x00, 0x00, 0x00 ; SIADDR
.db 0x00, 0x00, 0x00, 0x00 ; GIADDR
.db 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB ; CHADDR TODO fill MAC
.dw 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 ; For weird XXL MAC adress.
; OVERFLOW SPACE
.dw 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.dw 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.dw 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.dw 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.dw 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.dw 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.db 0x63, 0x82, 0x53, 0x63 ; Magic.
; DHCP Options.
.db 53, 1, 1, 61, 7, 1, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 50, 4, 192, 168, 2, 100, 12, 5, "hello", 55, 12, 1, 15, 3, 6, 44, 46, 47, 31, 33, 121, 249, 43, 0xFF,
.db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ; End option.
WNet_DHCPC_msg_discover_end:
