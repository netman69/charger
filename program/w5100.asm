;;;;;;;;;;;;;;;;;;
;; W5100 driver ;;
;;;;;;;;;;;;;;;;;;
;
; This has functions for talking to WizNet W5100 via SPI.
; We prefixed the functions WNet_ but named file w5100 in case
;   there's ever a compatible driver for another WizNet device.
; Sadly the W5100 does not tristate it's MISO pin.
;   For connecting it together with an SD card,
;     it appears to work to put a 500 ohm resistor in series with W5100 MISO.
;   There's also a pin to toggle MISO impedance on the w5100 but the board I have
;     doesn't break it out.
;

; Slave select pin.
.equ WNET_SS_DDR = DDRD
.equ WNET_SS_PORT = PORTD
.equ WNET_SS = 7

;
; WNet_init_port
;
;  Initialize SS line.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    -
;
WNet_init_port:
	sbi WNET_SS_DDR, WNET_SS
	sbi WNET_SS_PORT, WNET_SS ; SS high.
	ret

;
; WNet_init
;
;  Initialize the chip.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16, r17, r25:r25 trashed.
;
WNet_init:
	; TODO: Load from settings: Gateway, subnet, MAC, IP.
	clr r25 ; High byte is always 0 with common registers.
	; Initiate reset.
	ldi r17, WNET_MR_RST
	ldi r24, WNET_MR
	call WNet_write
	; Wait for reset to complete.
WNet_init_waitreset:
	call WNet_read
	cpi r16, 0x00
	brne WNet_init_waitreset
	; Gateway = 192.168.2.1
	ldi r17, 192
	ldi r24, WNET_GAR0
	call WNet_write
	ldi r17, 168
	ldi r24, WNET_GAR1
	call WNet_write
	ldi r17, 2
	ldi r24, WNET_GAR2
	call WNet_write
	ldi r17, 1
	ldi r24, WNET_GAR3
	call WNet_write
	; Subnet mask = 255.255.255.0
	ldi r17, 255
	ldi r24, WNET_SUBR0
	call WNet_write
	ldi r17, 255
	ldi r24, WNET_SUBR1
	call WNet_write
	ldi r17, 255
	ldi r24, WNET_SUBR2
	call WNet_write
	ldi r17, 0
	ldi r24, WNET_SUBR3
	call WNet_write
	; MAC = 01:23:45:67:89:ab
	ldi r17, 0x01
	ldi r24, WNET_SHAR0
	call WNet_write
	ldi r17, 0x23
	ldi r24, WNET_SHAR1
	call WNet_write
	ldi r17, 0x45
	ldi r24, WNET_SHAR2
	call WNet_write
	ldi r17, 0x67
	ldi r24, WNET_SHAR3
	call WNET_write	
	ldi r17, 0x89
	ldi r24, WNET_SHAR4
	call WNet_write	
	ldi r17, 0xAB
	ldi r24, WNET_SHAR5
	call WNet_write	
	; IP = 192.168.2.123
	ldi r17, 192
	ldi r24, WNET_SIPR0
	call WNet_write
	ldi r17, 168
	ldi r24, WNET_SIPR1
	call WNet_write
	ldi r17, 2
	ldi r24, WNET_SIPR2
	call WNet_write
	ldi r17, 123
	ldi r24, WNET_SIPR3
	call WNet_write
	ret

;
; WNet_listen
;
;  Listen on a TCP port.
;
;  Input:
;    r28: Socket number (0-3).
;    r19:r18: Port.
;
;  Output:
;    -
;
;  Side effects:
;    r16, r17, r25:r24, r29 trashed.
;
WNet_listen: ; TODO flexible socket #
	; Set TCP mode for Socket 0.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_MR
	ldi r17, WNET_Sn_MR_TCP
	call WNet_write
	; Set the source port.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_PORT0
	mov r17, r19
	call WNet_write
	adiw r25:r24, 1
	mov r17, r18
	call WNet_write
	; Open.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
	ldi r17, WNET_Sn_CR_OPEN
	call WNet_write
	; Check if that worked.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_SR
	call WNet_read
	cpi r16, WNET_Sn_SR_INIT
	brne WNet_listen_retry
	; Listen.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
	ldi r17, WNET_Sn_CR_LISTEN
	call WNet_write
	; Check if that worked.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_SR
	call WNet_read
	cpi r16, WNET_Sn_SR_LISTEN
	brne WNet_listen_retry
	; Success.
	ret
WNet_listen_retry: ; Close and try again.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
	ldi r17, WNET_Sn_CR_CLOSE
	call WNet_write
	rjmp WNet_listen

;
; WNet_udpopen
;
;  Open a UDP port.
;
;  Input:
;    r28: Socket number (0-3).
;    r19:r18: Port.
;
;  Output:
;    -
;
;  Side effects:
;    r16, r17, r25:r24, r29 trashed.
;
WNet_UDPopen:
	; Set UDP mode.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_MR
	ldi r17, WNET_Sn_MR_UDP
	call WNet_write
	; Set the source port.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_PORT0
	mov r17, r19
	call WNet_write
	adiw r25:r24, 1
	mov r17, r18
	call WNet_write
	; Open.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
	ldi r17, WNET_Sn_CR_OPEN
	call WNet_write
	; Check if that worked.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_SR
	call WNet_read
	cpi r16, WNET_Sn_SR_UDP
	breq WNet_udpopen_ok
		; Close and try again.
		call WNet_Sn_lookup
		adiw r25:r24, WNET_Sn_CR
		ldi r17, WNET_Sn_CR_CLOSE
		call WNet_write
		rjmp WNet_udpopen
WNet_udpopen_ok:
	ret

;
; WNet_getstatus
;
;  Get socket SR register.
;
;  Input:
;    r28: Socket number (0-3).
;
;  Output:
;    r16: SR register value.
;
;  Side effects:
;    r17, r25:r24, r29 trashed.
;
WNet_getstatus:
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_SR
	call WNet_read
	ret

;
; WNet_getreceived
;
;  Get amount of data received.
;
;  Input:
;    r28: Socket number (0-3).
;
;  Output:
;    r19:r18: Length of data received.
;
;  Side effects:
;    r16, r17, r25:r24, r29 trashed.
;
WNet_getreceived:
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_RSR0
	call WNet_read
	mov r19, r16
	adiw r25:r24, 1
	call WNet_read
	mov r18, r16
	ret

;
; WNet_disconnect
;
;  Disconnect a client.
;
;  Input:
;    r28: Socket number (0-3).
;
;  Output:
;    -
;
;  Side effects:
;    r16, r17, r25:r24, r29 trashed.
;
WNet_disconnect:
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
	ldi r17, WNET_Sn_CR_DISCON
	call WNet_write
.if 0 ; Waiting for disconnect done hangs if no connection exists, and seems pointless.
WNet_disconnect_wait:
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_SR
	call WNet_read
	cpi r16, WNET_Sn_SR_CLOSED
	brne WNet_disconnect_wait
.endif
	ret

;
; WNet_send_sram
;
;  Send a packet.
;
;  Input:
;    r28: Socket number (0-3).
;    Z (r31:r30): The data in program memory.
;    r19:r18: Length.
;
;  Output:
;    Z (r31:r30): After data sent.
;
;  Side effects:
;    r16, r17, r19:r18, r21:r20, r23:r22, r25:r24, r29 trashed.
;
;  Notes:
;    Size must be smaller than buffer and greater than 0 or function will hang.
;
WNet_send_sram: ; TODO write from SRAM option (or mandatory).
	; Register map of this function:
	;   r19:r18: Length of data to send.
	;   r21:r20: Offset in w5100 memory.
	;   r23:r22: Start adress in w5100 memory.
	; Wait until enough space in buffer.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_TX_FSR0
	call WNet_read
	mov r20, r16
	adiw r25:r24, 1
	call WNet_read
	cp r16, r18
	cpc r20, r19
	brcs WNet_send_sram
WNet_send_sram_start:
	; Calculate offset address.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_TX_WR0
	call WNet_read
	mov r21, r16
	mov r23, r16 ; Store Sn_TX_WR too.
	adiw r25:r24, 1
	call WNet_read
	mov r20, r16
	mov r22, r16
	call WNet_gSn_TX_MASK_lookup
	and r21, r25
	and r20, r24
	; Sn_TX_WR += length.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_TX_WR0
	add r22, r18
	adc r23, r19
	mov r17, r23
	call WNet_write
	adiw r25:r24, 1
	mov r17, r22
	call WNet_write
	; Calculate start adress.
	call WNet_gSn_TX_BASE_lookup
	mov r23, r25
	mov r22, r24
	add r22, r20
	adc r23, r21
	; Check if TX memory would overflow.
	call WNet_gSn_TX_MASK_lookup
	adiw r25:r24, 1
	mov r17, r21 ; if (offset + length) > (gSn_TX_MASK + 1)
	mov r16, r20
	add r16, r18
	adc r17, r19 ; r25:r24 = offset + length.
	cp r24, r16
	cpc r25, r17
	brcs WNet_send_sram_overflow
WNet_send_sram_nooverflow:
	; Write the whole thing straight, and we're done.
	mov r25, r23
	mov r24, r22
	call WNet_write_sram
	rjmp WNet_send_sram_done
WNet_send_sram_overflow:
	; Store r19:r18.
	push r19
	push r18
	; Calculate upper_size.
	call WNet_gSn_TX_MASK_lookup
	adiw r25:r24, 1
	mov r19, r25
	mov r18, r24
	sub r18, r20
	sbc r19, r21
	; Write upper_size bytes to start adress.
	mov r25, r23
	mov r24, r22
	call WNet_write_sram ; This increments Z appropriately for next step.
	; Get size_left.
	pop r16
	pop r17
	sub r16, r18
	sbc r17, r19
	mov r19, r17
	mov r18, r16
	; Write size_left bytes to gSn_TX_BASE.
	call WNet_gSn_TX_BASE_lookup
	call WNet_write_sram
WNet_send_sram_done:
	; Write send command.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
	ldi r17, WNET_Sn_CR_SEND
	call WNet_write
	ret

;
; WNet_send_pmem
;
;  Send a packet.
;
;  Input:
;    r28: Socket number (0-3).
;    Z (r31:r30): The data in program memory.
;    r19:r18: Length.
;
;  Output:
;    Z (r31:r30): After data sent.
;
;  Side effects:
;    r16, r17, r19:r18, r21:r20, r23:r22, r25:r24, r29 trashed.
;
;  Notes:
;    Size must be smaller than buffer and greater than 0 or function will hang.
;
WNet_send_pmem: ; TODO write from SRAM option (or mandatory).
	; Register map of this function:
	;   r19:r18: Length of data to send.
	;   r21:r20: Offset in w5100 memory.
	;   r23:r22: Start adress in w5100 memory.
	; Wait until enough space in buffer.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_TX_FSR0
	call WNet_read
	mov r20, r16
	adiw r25:r24, 1
	call WNet_read
	cp r16, r18
	cpc r20, r19
	brcs WNet_send_pmem
WNet_send_pmem_start:
	; Calculate offset address.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_TX_WR0
	call WNet_read
	mov r21, r16
	mov r23, r16 ; Store Sn_TX_WR too.
	adiw r25:r24, 1
	call WNet_read
	mov r20, r16
	mov r22, r16
	call WNet_gSn_TX_MASK_lookup
	and r21, r25
	and r20, r24
	; Sn_TX_WR += length.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_TX_WR0
	add r22, r18
	adc r23, r19
	mov r17, r23
	call WNet_write
	adiw r25:r24, 1
	mov r17, r22
	call WNet_write
	; Calculate start adress.
	call WNet_gSn_TX_BASE_lookup
	mov r23, r25
	mov r22, r24
	add r22, r20
	adc r23, r21
	; Check if TX memory would overflow.
	call WNet_gSn_TX_MASK_lookup
	adiw r25:r24, 1
	mov r17, r21 ; if (offset + length) > (gSn_TX_MASK + 1)
	mov r16, r20
	add r16, r18
	adc r17, r19 ; r25:r24 = offset + length.
	cp r24, r16
	cpc r25, r17
	brcs WNet_send_pmem_overflow
WNet_send_pmem_nooverflow:
	; Write the whole thing straight, and we're done.
	mov r25, r23
	mov r24, r22
	call WNet_write_pmem
	rjmp WNet_send_pmem_done
WNet_send_pmem_overflow:
	; Store r19:r18.
	push r19
	push r18
	; Calculate upper_size.
	call WNet_gSn_TX_MASK_lookup
	adiw r25:r24, 1
	mov r19, r25
	mov r18, r24
	sub r18, r20
	sbc r19, r21
	; Write upper_size bytes to start adress.
	mov r25, r23
	mov r24, r22
	call WNet_write_pmem ; This increments Z appropriately for next step.
	; Get size_left.
	pop r16
	pop r17
	sub r16, r18
	sbc r17, r19
	mov r19, r17
	mov r18, r16
	; Write size_left bytes to gSn_TX_BASE.
	call WNet_gSn_TX_BASE_lookup
	call WNet_write_pmem
WNet_send_pmem_done:
	; Write send command.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
	ldi r17, WNET_Sn_CR_SEND
	call WNet_write
	ret

;
; WNet_recv
;
;  Receive data.
;
;  Input:
;    r28: Socket number (0-3).
;    Z (r31:r30): Where to store that shit.
;    r19:r18: Length.
;
;  Output:
;    Z (r31:r30): Incremented to end of received shit.
;
;  Side effects:
;    r16, r17, r19:r18, r21:r20, r23:r22, r25:r24, r29 trashed.
;
;  Notes:
;    Size must be greater than 0.
;    If size is bigger than already received data, function will hang until enough data ready.
;
WNet_recv:
	; Register map of this function:
	;   r19:r18: Length of data to receive.
	;   r21:r20: Offset in w5100 memory.
	;   r23:r22: Start adress in w5100 memory.
	; Wait until enough bytes received.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_RSR0
	call WNet_read
	mov r20, r16
	adiw r25:r24, 1
	call WNet_read
	cp r16, r18
	cpc r20, r19
	brcs WNet_recv
WNet_recv_start:
	; Calculate offset address.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_RX_RD0
	call WNet_read
	mov r21, r16
	mov r23, r16 ; Store Sn_RX_RD too.
	adiw r25:r24, 1
	call WNet_read
	mov r20, r16
	mov r22, r16
	call WNet_gSn_RX_MASK_lookup
	and r21, r25
	and r20, r24
	; Sn_RX_RD += length.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_RX_RD0
	add r22, r18
	adc r23, r19
	mov r17, r23
	call WNet_write
	adiw r25:r24, 1
	mov r17, r22
	call WNet_write
	; Calculate start adress.
	call WNet_gSn_RX_BASE_lookup
	mov r23, r25
	mov r22, r24
	add r22, r20
	adc r23, r21
	; Check if RX memory would overflow.
	call WNet_gSn_RX_MASK_lookup
	adiw r25:r24, 1
	mov r17, r21 ; if (offset + length) > (gSn_TX_MASK + 1)
	mov r16, r20
	add r16, r18
	adc r17, r19 ; r25:r24 = offset + length.
	cp r24, r16
	cpc r25, r17
	brcs WNet_recv_overflow
WNet_recv_nooverflow:
	; Write the whole thing straight, and we're done.
	mov r25, r23
	mov r24, r22
	call WNet_read_sram
	rjmp WNet_recv_done
WNet_recv_overflow:
	; Store r19:r18.
	push r19
	push r18
	; Calculate upper_size.
	call WNet_gSn_RX_MASK_lookup
	adiw r25:r24, 1
	mov r19, r25
	mov r18, r24
	sub r18, r20
	sbc r19, r21
	; Write upper_size bytes to start adress.
	mov r25, r23
	mov r24, r22
	call WNet_read_sram ; This increments Z appropriately for next step.
	; Get size_left.
	pop r16
	pop r17
	sub r16, r18
	sbc r17, r19
	mov r19, r17
	mov r18, r16
	; Write size_left bytes to gSn_TX_BASE.
	call WNet_gSn_RX_BASE_lookup
	call WNet_read_sram
WNet_recv_done:
	; Write recv command.
	call WNet_Sn_lookup
	adiw r25:r24, WNET_Sn_CR
	ldi r17, WNET_Sn_CR_RECV
	call WNet_write
	ret

;
; WNet_write_pmem
;
;  Write a bunch of data from program memory to chip.
;
;  Input:
;    r25:r24: Starting adress (in the WizNet chip).
;    Z (r31:r30): The data in program memory.
;    r19:r18: Length.
;
;  Output:
;    Z (r31:r30): incremented as far as data sent.
;
;  Side effects:
;    r16, r17, r25:r24 trashed.
;
;  Notes:
;    r20 must be > 0.
;
WNet_write_pmem:
	push r19
	push r18
	add r18, r24
	adc r19, r25
WNet_write_pmem_loop:
	lpm r17, Z+
	call WNet_write
	adiw r25:r24, 1
	cp r24, r18
	cpc r25, r19
	brne WNet_write_pmem_loop
	pop r18
	pop r19
	ret

;
; WNet_write_sram
;
;  Write a bunch of data from SRAM to chip.
;
;  Input:
;    r25:r24: Starting adress (in the WizNet chip).
;    Z (r31:r30): The data.
;    r19:r18: Length.
;
;  Output:
;    Z (r31:r30): incremented as far as data sent.
;
;  Side effects:
;    r16, r17, r25:r24 trashed.
;
;  Notes:
;    r20 must be > 0.
;
WNet_write_sram:
	push r19
	push r18
	add r18, r24
	adc r19, r25
WNet_write_sram_loop:
	ld r17, Z+
	call WNet_write
	adiw r25:r24, 1
	cp r24, r18
	cpc r25, r19
	brne WNet_write_sram_loop
	pop r18
	pop r19
	ret

;
; WNet_read_sram
;
;  Read a bunch of data from chip to SRAM.
;
;  Input:
;    r25:r24: Starting adress (in the WizNet chip).
;    Z (r31:r30): The data target adress.
;    r19:r18: Length.
;
;  Output:
;    Z (r31:r30): incremented as far as data received.
;
;  Side effects:
;    r16, r17, r25:r24 trashed.
;
;  Notes:
;    r20 must be > 0.
;
WNet_read_sram:
	push r19
	push r18
	add r18, r24
	adc r19, r25
WNet_read_sram_loop:
	call WNet_read
	st Z+, r16
	adiw r25:r24, 1
	cp r24, r18
	cpc r25, r19
	brne WNet_read_sram_loop
	pop r18
	pop r19
	ret

;
; WNet_write
;
;  Write a byte in the chips memory.
;
;  Input:
;    r17: The byte.
;    r25:r24: Adress (in the WizNet chip).
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
WNet_write:
	cbi WNET_SS_PORT, WNET_SS ; SS low.
	ldi r16, 0xF0
	call SPI_send
	mov r16, r25
	call SPI_send
	mov r16, r24
	call SPI_send
	mov r16, r17
	call SPI_send
	sbi WNET_SS_PORT, WNET_SS ; SS high.
	ret

;
; WNet_read
;
;  Read a byte from the chips memory.
;
;  Input:
;    r25:r24: Adress (in the WizNet chip).
;
;  Output:
;    r16: The byte.
;
;  Side effects:
;    -
;
WNet_read:
	cbi WNET_SS_PORT, WNET_SS ; SS low.
	ldi r16, 0x0F
	call SPI_send
	mov r16, r25
	call SPI_send
	mov r16, r24
	call SPI_send
	call SPI_recv
	sbi WNET_SS_PORT, WNET_SS ; SS high.
	ret

;
; WNet_Sn_lookup, WNet_gSn_TX_BASE_lookup, WNet_gSn_TX_MASK_lookup,
;   WNet_gSn_RX_BASE_lookup, WNet_gSn_RX_MASK_lookup
;
;  Look up socket registers, or memory location.
;
;  Input:
;    r28: Socket number (0-3).
;
;  Output:
;    r25:r24: Adress (in the WizNet chip).
;
;  Side effects:
;    r29 trashed.
;
WNet_gSn_TX_BASE_lookup:
	push r31
	push r30
	ldi r31, high(WNET_gSn_TX_BASE)
	ldi r30, low(WNET_gSn_TX_BASE)
	rjmp WNet_xx_lookup
WNet_gSn_TX_MASK_lookup:
	push r31
	push r30
	ldi r31, high(WNET_gSn_TX_MASK)
	ldi r30, low(WNET_gSn_TX_MASK)
	rjmp WNet_xx_lookup
WNet_gSn_RX_BASE_lookup:
	push r31
	push r30
	ldi r31, high(WNET_gSn_RX_BASE)
	ldi r30, low(WNET_gSn_RX_BASE)
	rjmp WNet_xx_lookup
WNet_gSn_RX_MASK_lookup:
	push r31
	push r30
	ldi r31, high(WNET_gSn_RX_MASK)
	ldi r30, low(WNET_gSn_RX_MASK)
	rjmp WNet_xx_lookup
WNet_Sn_lookup:
	push r31
	push r30
	ldi r31, high(WNET_Sn)
	ldi r30, low(WNET_Sn)
WNet_xx_lookup:
	clr r29
	add r30, r28
	adc r31, r29
	lsl r30
	rol r31
	lpm r24, Z+
	lpm r25, Z+
	pop r30
	pop r31
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Common register adresses ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.equ WNET_MR     = 0x0000 ; Mode
.equ WNET_GAR0   = 0x0001 ; Gateway Adress
.equ WNET_GAR1   = 0x0002
.equ WNET_GAR2   = 0x0003
.equ WNET_GAR3   = 0x0004
.equ WNET_SUBR0  = 0x0005 ; Subnet Mask
.equ WNET_SUBR1  = 0x0006
.equ WNET_SUBR2  = 0x0007
.equ WNET_SUBR3  = 0x0008
.equ WNET_SHAR0  = 0x0009 ; Source Hardware Adress (MAC)
.equ WNET_SHAR1  = 0x000A
.equ WNET_SHAR2  = 0x000B
.equ WNET_SHAR3  = 0x000C
.equ WNET_SHAR4  = 0x000D
.equ WNET_SHAR5  = 0x000E
.equ WNET_SIPR0  = 0x000F ; Source IP Adress
.equ WNET_SIPR1  = 0x0010
.equ WNET_SIPR2  = 0x0011
.equ WNET_SIPR3  = 0x0012
.equ WNET_IR     = 0x0015 ; Interrupt Flags
.equ WNET_IMR    = 0x0016 ; Interrupt Mask
.equ WNET_RTR0   = 0x0017 ; Retry Time
.equ WNET_RTR1   = 0x0018
.equ WNET_RCR    = 0x0019 ; RCR
.equ WNET_RMSR   = 0x001A ; RX Memory Size
.equ WNET_TMSR   = 0x001B ; TX Memory Size
; PPoE and PPP shit left out.
.equ WNET_UIPR0  = 0x002A ; Unreachable IP Adress
.equ WNET_UIPR1  = 0x002B
.equ WNET_UIPR2  = 0x002C
.equ WNET_UIPR3  = 0x002D
.equ WNET_UPORT0 = 0x002E ; Unreachable Port
.equ WNET_UPORT1 = 0x002E
; Flags/values for these register adresses, some (PPoE / non-SPI) omitted.
.equ WNET_MR_RST         = (1 << 7)
.equ WNET_MR_PB          = (1 << 4)
.equ WNET_IR_CONFLICT    = (1 << 7)
.equ WNET_IR_UNCREACH    = (1 << 6)
.equ WNET_IR_S3_INT      = (1 << 3)
.equ WNET_IR_S2_INT      = (1 << 2)
.equ WNET_IR_S1_INT      = (1 << 1)
.equ WNET_IR_S0_INT      = (1 << 0)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Socket register adresses ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.equ WNET_S0    = 0x0400
.equ WNET_S1    = 0x0500
.equ WNET_S2    = 0x0600
.equ WNET_S3    = 0x0700
; One of above + one of below = an actual register adress.
.equ WNET_Sn_MR      = 0x0000 ; Mode
.equ WNET_Sn_CR      = 0x0001 ; Command
.equ WNET_Sn_IR      = 0x0002
.equ WNET_Sn_SR      = 0x0003
.equ WNET_Sn_PORT0   = 0x0004 ; Source Port
.equ WNET_Sn_PORT1   = 0x0005
.equ WNET_Sn_DHAR0   = 0x0006
.equ WNET_Sn_DHAR1   = 0x0007
.equ WNET_Sn_DHAR2   = 0x0008
.equ WNET_Sn_DHAR3   = 0x0009
.equ WNET_Sn_DHAR4   = 0x000A
.equ WNET_Sn_DHAR5   = 0x000B
.equ WNET_Sn_DIPR0   = 0x000C
.equ WNET_Sn_DIPR1   = 0x000D
.equ WNET_Sn_DIPR2   = 0x000E
.equ WNET_Sn_DIPR3   = 0x000F
.equ WNET_Sn_DPORT0  = 0x0010
.equ WNET_Sn_DPORT1  = 0x0011
.equ WNET_Sn_MSSR0   = 0x0012
.equ WNET_Sn_MSSR1   = 0x0013
.equ WNET_Sn_PROTO   = 0x0014
.equ WNET_Sn_TOS     = 0x0015
.equ WNET_Sn_TTL     = 0x0016
.equ WNET_Sn_TX_FSR0 = 0x0020
.equ WNET_Sn_TX_FSR1 = 0x0021
.equ WNET_Sn_TX_RD0  = 0x0022
.equ WNET_Sn_TX_RD1  = 0x0023
.equ WNET_Sn_TX_WR0  = 0x0024
.equ WNET_Sn_TX_WR1  = 0x0025
.equ WNET_Sn_RSR0    = 0x0026
.equ WNET_Sn_RSR1    = 0x0027
.equ WNET_Sn_RX_RD0  = 0x0028
.equ WNET_Sn_RX_RD1  = 0x0029
; Flags/values for these register adresses, some (PPoE etc) omitted.
.equ WNET_Sn_MR_MULTI          = (1 << 7)
.equ WNET_Sn_MR_MF             = (1 << 6)
.equ WNET_Sn_MR_NDMC           = (1 << 6)
.equ WNET_Sn_MR_CLOSED         = 0x00
.equ WNET_Sn_MR_TCP            = 0x01
.equ WNET_Sn_MR_UDP            = 0x02
.equ WNET_Sn_MR_IPRAW          = 0x03
.equ WNET_Sn_MR_MACRAW         = 0x04
.equ WNET_Sn_CR_OPEN           = 0x01
.equ WNET_Sn_CR_LISTEN         = 0x02
.equ WNET_Sn_CR_CONNECT        = 0x04
.equ WNET_Sn_CR_DISCON         = 0x08
.equ WNET_Sn_CR_CLOSE          = 0x10
.equ WNET_Sn_CR_SEND           = 0x20
.equ WNET_Sn_CR_SEND_MAC       = 0x21
.equ WNET_Sn_CR_SEND_KEEP      = 0x22
.equ WNET_Sn_CR_RECV           = 0x40
.equ WNET_Sn_IR_SEND_OK        = (1 << 4)
.equ WNET_Sn_IR_TIMEOUT        = (1 << 3)
.equ WNET_Sn_IR_RECV           = (1 << 2)
.equ WNET_Sn_IR_DISCON         = (1 << 1)
.equ WNET_Sn_IR_CON            = (1 << 0)
.equ WNET_Sn_SR_CLOSED         = 0x00
.equ WNET_Sn_SR_INIT           = 0x13
.equ WNET_Sn_SR_LISTEN         = 0x14
.equ WNET_Sn_SR_ESTABLISHED    = 0x17
.equ WNET_Sn_SR_CLOSE_WAIT     = 0x1C
.equ WNET_Sn_SR_UDP            = 0x22
.equ WNET_Sn_SR_IPRAW          = 0x32
.equ WNET_Sn_SR_MACRAW         = 0x42
.equ WNET_Sn_SR_SYNSENT        = 0x15
.equ WNET_Sn_SR_SYNRECV        = 0x16
.equ WNET_Sn_SR_FIN_WAIT       = 0x18
.equ WNET_Sn_SR_CLOSING        = 0x1A
.equ WNET_Sn_SR_TIME_WAIT      = 0x1B
.equ WNET_Sn_SR_LAST_ACK       = 0x1D
.equ WNET_Sn_SR_SOCK_ARP       = 0x01
; These constants depend on TMSR and RMSR = 0x55 (default value, 2k/socket).
.equ WNET_gS0_TX_BASE = 0x4000
.equ WNET_gS0_TX_MASK = 0x07FF
.equ WNET_gS1_TX_BASE = 0x4800
.equ WNET_gS1_TX_MASK = 0x07FF
.equ WNET_gS2_TX_BASE = 0x5000
.equ WNET_gS2_TX_MASK = 0x07FF
.equ WNET_gS3_TX_BASE = 0x5800
.equ WNET_gS3_TX_MASK = 0x07FF
.equ WNET_gS0_RX_BASE = 0x6000
.equ WNET_gS0_RX_MASK = 0x07FF
.equ WNET_gS1_RX_BASE = 0x6800
.equ WNET_gS1_RX_MASK = 0x07FF
.equ WNET_gS2_RX_BASE = 0x7000
.equ WNET_gS2_RX_MASK = 0x07FF
.equ WNET_gS3_RX_BASE = 0x7800
.equ WNET_gS3_RX_MASK = 0x07FF

; Lookup tables for socket info.
WNET_Sn:
.dw WNET_S0, WNET_S1, WNET_S2, WNET_S3
WNET_gSn_TX_BASE:
.dw WNET_gS0_TX_BASE, WNET_gS1_TX_BASE, WNET_gS2_TX_BASE, WNET_gS3_TX_BASE
WNET_gSn_RX_BASE:
.dw WNET_gS0_RX_BASE, WNET_gS1_RX_BASE, WNET_gS2_RX_BASE, WNET_gS3_RX_BASE
WNET_gSn_TX_MASK:
.dw WNET_gS0_TX_MASK, WNET_gS1_TX_MASK, WNET_gS2_TX_MASK, WNET_gS3_TX_MASK
WNET_gSn_RX_MASK:
.dw WNET_gS0_RX_MASK, WNET_gS1_RX_MASK, WNET_gS2_RX_MASK, WNET_gS3_RX_MASK
