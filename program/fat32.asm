;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; FAT32 driver.                                             ;;
;;                                                           ;;
;; This file needs memory.asm to assemble (described below). ;;
;; Also it needs SD_read, write, etc functions from sd.asm.  ;;
;; It assumes SD_init has been called.                       ;;
;;                                                           ;;
;; Due to simplicity of the driver, if a lot of directory    ;;
;;   entries are created, the clusters allocated don't get   ;;
;;   released automatically. Windows 7 also shows this       ;;
;;   behaviour.                                              ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; Usage:
;
;   To open a filesystem, we need to call FAT_mount.
;
;   Dealing with directories works as follows:
;     FAT_open: Open a directory or a file in it.
;     FAT_creat: Create a file inside a directory.
;     FAT_unlink: Delete a file from a directory.
;     FAT_mkdir: Create directories.
;
;   After that, for dealing with files we can use:
;     FAT_read: Read data from a file.
;     FAT_write: Write data to a file.
;     FAT_trunc: Shorten a file.
;     FAT_seek: Seek forward in a file.
;     FAT_seek_start: Go to start of file.
;     FAT_seek_end: Go to end of file.
;

; On error of the functions that can error, carry will be set.
; Also r16 will be loaded with the err no.
; Additional error numbers are defined in sd.asm.
.equ ERR_EOF = 0x81 ; Errors under 0x80 are reserved for sd.asm.
.equ ERR_MNT = 0x82 ; Mounting failed.
.equ ERR_FNF = 0x83 ; File not found.
.equ ERR_FSF = 0x84 ; Filesystem full.

; These are offsets starting from FAT_CFG
;   where we need 19 bytes of SRAM storage for globals.
; All of these are stored in big-endian.
.equ FAT_START = FAT_CFG + 0  ; dword 4 - Start of filesystem.
.equ FAT_SPC   = FAT_CFG + 4  ; byte  1 - Sectors / cluster.
.equ FAT_CPY   = FAT_CFG + 5  ; byte  1 - FAT copies.
.equ FAT_SPF   = FAT_CFG + 6  ; dword 4 - Sectors per fat.
.equ FAT_FLAGS = FAT_CFG + 10 ; byte  1 - Mirorring flags.
.equ FAT_FCL   = FAT_CFG + 11 ; dword 4 - First cluster with file data (sectors until).
.equ FAT_AFAT  = FAT_CFG + 15 ; dword 4 - Start of the "active fat".
.equ FAT_CFS   = FAT_CFG + 19 ; dword 4 - Current sector of fat table that we'd search for new clusters.
; FAT_CFG total size            =     23 bytes
;
; We could make fat mirroring implied always on and not need FAT_FLAGS.
; But then what if the first fat is not the primary one?

; These are relative to a "file handle" 22 bytes long.
; This library also needs FAT_RFN equ'd to a SRAM location for the root file handle.
.equ FN_DFSE = 0  ; dword: Directory First SEctor.
.equ FN_DOFF = 4  ; word: Directory entry OFFset (in DFSE, in bytes).
.equ FN_FCL  = 6  ; dword: First CLuster.
.equ FN_CCL  = 10 ; dword: Current CLuster. Bit 31 set if we should make a new cluster before writing.
.equ FN_POS  = 14 ; dword: POSition.
.equ FN_LEN  = 18 ; dword: LENgth.
.equ FN_SIZE = 22 ; The size of a file handle.
; Order of these matters for some functions.
; I try to mark these with the word FNORDER.

;
; FAT_mount
;
;  Try to mount FAT32 filesystem on the SD card.
;
;  Input:
;    -
;
;  Output:
;    C flag set on error (and r16 loaded with err no).
;
;  Side effects:
;    r0, r1, r16, r17, r18, r19, r20, r21, SD_BUF (SRAM), r31:r30 (Z) trashed.
;
FAT_mount:
	; Clear r17-r20
	call SD_CMD_clarg
FAT_mount_try:
	; Store what we guessed is the start of filesystem.
	sts FAT_START + 0, r20
	sts FAT_START + 1, r19
	sts FAT_START + 2, r18
	sts FAT_START + 3, r17
	call SD_read
	brcs FAT_mount_end
	; Check 55AA signature.
	lds r16, SD_BUF + 510
	cpi r16, 0x55
	brne FAT_mount_fail ; Fail if this is not an MBR.
	lds r16, SD_BUF + 511
	cpi r16, 0xAA
	brne FAT_mount_fail
	; Check if this is a FAT partition or just MBR. This seems a bit ugly.
	lds r16, SD_BUF + 0x52 + 0
	cpi r16, 'F'
	brne FAT_mount_findpart
	lds r16, SD_BUF + 0x52 + 1
	cpi r16, 'A'
	brne FAT_mount_findpart
	lds r16, SD_BUF + 0x52 + 2
	cpi r16, 'T'
	brne FAT_mount_findpart
	lds r16, SD_BUF + 0x52 + 3
	cpi r16, '3'
	brne FAT_mount_findpart
	lds r16, SD_BUF + 0x52 + 4
	cpi r16, '2'
	brne FAT_mount_findpart
	lds r16, SD_BUF + 0x52 + 5
	cpi r16, ' '
	brne FAT_mount_findpart
	lds r16, SD_BUF + 0x52 + 6
	cpi r16, ' '
	brne FAT_mount_findpart
	lds r16, SD_BUF + 0x52 + 7
	cpi r16, ' '
	brne FAT_mount_findpart
	rjmp FAT_mount_ispart ; _findpart and _fail where at the bottom but then it's too far for breq/brne
	; Bail out.
FAT_mount_fail:
	ldi r16, ERR_MNT
	sec
FAT_mount_end:
	ret
	; Attempt to find partition.
FAT_mount_findpart:
	; Check if not we already tried to find a partition.
	clr r16
	lds r17, FAT_START + 3
	or r16, r17
	lds r17, FAT_START + 2
	or r16, r17
	lds r17, FAT_START + 1
	or r16, r17
	lds r17, FAT_START + 0
	or r16, r17
	brne FAT_mount_fail ; If any bits were set in r17-r19 Z flag is cleared.
	; Check if the first partition is fat, and if so try to use it.
	lds r16, SD_BUF + 0x1BE + 4 ; 0x1BE is first partition in table.
	cpi r16, 0x0B ; 0x0B or 0x0C partition type is FAT32
	breq FAT_mount_partok
	cpi r16, 0x0C
	breq FAT_mount_partok
	rjmp FAT_mount_fail
FAT_mount_partok:
	; Load the adress of the partition and retry.
	lds r17, SD_BUF + 0x1BE + 8 + 0 ; LSB
	lds r18, SD_BUF + 0x1BE + 8 + 1
	lds r19, SD_BUF + 0x1BE + 8 + 2
	lds r20, SD_BUF + 0x1BE + 8 + 3 ; MSB
	mov r16, r17 ; Check if the LBA is zero.
	or r16, r18
	or r16, r19
	or r16, r20
	breq FAT_mount_fail ; CHS not supported yet.
	rjmp FAT_mount_try
	; Attempt to mount partition. We only reach here if we have a fat partition.
FAT_mount_ispart:
	; Check if bytes/sector is 512, so we don't destroy stuff on weird FS.
	lds r16, SD_BUF + 0x0B + 0 ; LSB.
	cpi r16, 0
	brne FAT_mount_fail
	lds r16, SD_BUF + 0x0B + 1 ; MSB.
	cpi r16, 2
	brne FAT_mount_fail
	; Sectors/cluster. We can optionally check if this is indeed a power of 2.
	lds r16, SD_BUF + 0x0D
	sts FAT_SPC, r16
	; Reserved sectors count.
	lds r16, FAT_START + 3
	lds r17, SD_BUF + 0x0E + 0 ; LSB.
	add r16, r17
	sts FAT_START + 3, r16
	lds r16, FAT_START + 2
	lds r17, SD_BUF + 0x0E + 1 ; MSB.
	adc r16, r17
	sts FAT_START + 2, r16
	lds r16, FAT_START + 1
	clr r17 ; Just for carry.
	adc r16, r17
	sts FAT_START + 1, r16
	; Number of FAT copies.
	lds r16, SD_BUF + 0x10
	sts FAT_CPY, r16
	; Sectors per fat.
	lds r16, SD_BUF + 0x24 + 0 ; LSB
	sts FAT_SPF + 3, r16
	lds r16, SD_BUF + 0x24 + 1
	sts FAT_SPF + 2, r16
	lds r16, SD_BUF + 0x24 + 2
	sts FAT_SPF + 1, r16
	lds r16, SD_BUF + 0x24 + 3 ; MSB
	sts FAT_SPF + 0, r16
	; Mirroring flags
	lds r16, SD_BUF + 0x28
	sts FAT_FLAGS, r16
	; Calculate first cluster that has file data (FAT_SPF * FAT_CPY + FAT_START).
	clr r20
	clr r19
	lds r16, FAT_CPY
	lds r21, FAT_SPF + 3
	mul r16, r21
	mov r18, r1
	mov r17, r0
	lds r21, FAT_SPF + 2
	mul r16, r21
	add r18, r0
	adc r19, r1
	lds r21, FAT_SPF + 1
	mul r16, r21
	add r19, r0
	adc r20, r1
	lds r21, FAT_SPF + 0
	mul r16, r21
	add r20, r0
	lds r16, FAT_START + 3 ; Add FAT_START while storing in FAT_FCL
	add r16, r17
	sts FAT_FCL + 3, r16
	lds r16, FAT_START + 2
	adc r16, r18
	sts FAT_FCL + 2, r16
	lds r16, FAT_START + 1
	adc r16, r19
	sts FAT_FCL + 1, r16
	lds r16, FAT_START + 0
	adc r16, r20
	sts FAT_FCL + 0, r16
	; Find the "active" FAT and store its sector number.
	; NOTE With fat mirroring it shouldn't really matter which one is used.
	lds r20, FAT_START + 0
	lds r19, FAT_START + 1
	lds r18, FAT_START + 2
	lds r17, FAT_START + 3
	lds r16, FAT_FLAGS ; Now multiply this by FAT_SPF and add.
	andi r16, 0x0F
	lds r21, FAT_SPF + 3
	mul r16, r21
	add r17, r0
	adc r18, r1
	lds r21, FAT_SPF + 2
	mul r16, r21
	add r18, r0
	adc r19, r1
	lds r21, FAT_SPF + 1
	mul r16, r21
	add r19, r0
	adc r20, r1
	lds r21, FAT_SPF + 0
	mul r16, r21
	add r20, r0
	sts FAT_AFAT + 0, r20
	sts FAT_AFAT + 1, r19
	sts FAT_AFAT + 2, r18
	sts FAT_AFAT + 3, r17
	; Clear FAT_CFS.
	clr r16
	sts FAT_CFS + 0, r16
	sts FAT_CFS + 1, r16
	sts FAT_CFS + 2, r16
	sts FAT_CFS + 3, r16
	; Clear the root file handle.
	ldi r16, FN_SIZE
	clr r17
	ldi r31, high(FAT_RFN)
	ldi r30, low(FAT_RFN)
FAT_mount_rootclr:
	st Z+, r17
	dec r16
	brne FAT_mount_rootclr
	; Root folder cluster (into FAT_RFN FCL and CCL).
	lds r16, SD_BUF + 0x2C + 0 ; LSB
	sts FAT_RFN + FN_FCL + 3, r16
	sts FAT_RFN + FN_CCL + 3, r16
	lds r16, SD_BUF + 0x2C + 1
	sts FAT_RFN + FN_FCL + 2, r16
	sts FAT_RFN + FN_CCL + 2, r16
	lds r16, SD_BUF + 0x2C + 2
	sts FAT_RFN + FN_FCL + 1, r16
	sts FAT_RFN + FN_CCL + 1, r16
	lds r16, SD_BUF + 0x2C + 3 ; MSB
	andi r16, 0xF0 ; Only 28 bits used.
	sts FAT_RFN + FN_FCL + 0, r16
	sts FAT_RFN + FN_CCL + 0, r16
	clc
	ret

;
; FAT_open
;
;  Open a file by file name.
;
;  Input:
;    X (r27:r26): File handle destination (needs 24 bytes).
;    Y (r29:r28): File name.
;    Z (r31:r30): Directory file handle.
;
;  Output:
;    X (r27:r26): Original directory file handle.
;    Z (r31:r30): File handle destination copied.
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0, r1, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed, r29:r28 (Y) trashed.
;
FAT_open:
	push r27 ; File handle dst, we need X.
	push r26
	call FAT_seek_start
FAT_open_searchloop:
	; Read a sector.
	call FAT_read_sector
	brcc +1
		rjmp FAT_open_fail
	; Look for the file in that sector.
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
FAT_open_searchloop_file:
	ld r16, X+
	cpi r16, 0
	brne +1
		rjmp FAT_open_notfound ; End of directory.
	ldi r24, 11
FAT_open_searchloop_char:
	ld r17, Y+
	cp r16, r17 ; r16 got first byte already at first iteration.
	breq FAT_open_searchloop_charok
	; Fallthrough means char wrong, restore Y.
	subi r24, 12 ; r24 is 11 for Y+1, 10 for Y+2, etc.
	add r28, r24 ; For example if Y+2, r24 now is -2.
	ldi r24, 0xFF
	adc r29, r24
	; Go to next entry.
	adiw r27:r26, 32
	andi r26, ~31 ; Align to entry.
	; Check if this is the end of sector, if so, seek.
	cpi r27, high(SD_BUF + 512)
	brne FAT_open_noseek
	cpi r26, low(SD_BUF + 512)
	brne +1
		rjmp FAT_open_seek
FAT_open_noseek:
	rjmp FAT_open_searchloop_file
FAT_open_searchloop_charok:
	ld r16, X+ ; 2nd char on first iteration.
	dec r24
	brne FAT_open_searchloop_char
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Falling through means success. ;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	; Now we can begin filling the file handle.
	pop r28 ; File handle target.
	pop r29 ; We don't need Y (filename) anymore.
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; We start with FN_DFSE and FN_DOFF ;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	; Load FN_FCL of dir.
	adiw r31:r30, FN_FCL ; Z = Directory's first cluster in dir FN.
	ld r20, Z+ ; r20:..:r17 = FN_FCL
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	subi r17, 2 ; First cluster is second lol.
	clr r16
	sbc r18, r16
	sbc r19, r16
	sbc r20, r16
	; Now we do r24:..:r21 = FN_FCL * FAT_SPC.
	lds r16, FAT_SPC
	clr r24
	clr r23
	mul r17, r16
	mov r21, r0
	mov r22, r1
	mul r18, r16
	add r22, r0
	adc r23, r1
	mul r19, r16
	add r23, r0
	adc r24, r1
	mul r20, r16
	add r24, r0
	; Add FAT_FCL, start of actual clusters.
	lds r16, FAT_FCL + 3
	add r21, r16
	lds r16, FAT_FCL + 2
	adc r22, r16
	lds r16, FAT_FCL + 1
	adc r23, r16
	lds r16, FAT_FCL + 0
	adc r24, r16
	; Add FN_POS / 512, to r24:..:r21.
	adiw r31:r30, FN_POS - (FN_FCL + 4) ; FNORDER get dir at FN_POS.
	clr r20
	ld r19, Z+ ; Load r20:..r17 with FN_POS / 512.
	lsr r19
	ld r18, Z+
	ror r18
	ld r17, Z+
	ror r17
	sbiw r31:r30, FN_POS + 3 ; Restore Z.
	add r21, r17
	adc r22, r18
	adc r23, r19
	adc r24, r20
	; Store it to FN_DFSE.
	st Y+, r24 ; FNORDER Y is start of handle FN_DFSE.
	st Y+, r23
	st Y+, r22
	st Y+, r21
	; Subtract SD_BUF from X (in r17:r16), get offset of dir entry in FN_DFSE.
	mov r16, r26
	mov r17, r27
	subi r16, low(SD_BUF) ; Get X - SD_BUF.
	ldi r18, high(SD_BUF)
	sbc r17, r18
	andi r16, ~31 ; Align to start of dir entry.
	; Store FN_DOFF.
	st Y+, r17
	st Y+, r16 ; FNORDER We're expecting FN_FCL next.
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Now the dir entry is stored, store FCL, CCL, LEN, etc. ;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	; Store the Useful bits of dir entry in r17-r24.
	adiw r27:r26, 8 ; X is past filename +1. Already offset +1 due to weird loop.
	ld r19, X+ ; Cluster high word.
	ld r20, X+ ; Cluster high word.
	andi r20, 0xF0
	adiw r27:r26, 4 ; File time is here.
	ld r17, X+ ; Cluster low word.
	ld r18, X+ ; Cluster low word.
	ld r21, X+ ; File size LSB
	ld r22, X+
	ld r23, X+
	ld r24, X+ ; File size MSB
	sbiw r27:r26, 32 ; FNORDER Set X to start of dir entry.
	; Fill file first cluster FN_FCL.
	ldi r16, 2
FAT_open_fillcl: ; FNORDER We're expecting FN_FCL in Y.
	st Y+, r20 ; r20-r17 are first cluster.
	st Y+, r19
	st Y+, r18
	st Y+, r17
	dec r16 ; FNORDER We depend on FN_FCL and FN_CCL being consecutive.
	brne FAT_open_fillcl
	st Y+, r16 ; FNORDER This is where we wanna clear FN_POS.
	st Y+, r16 ; NOTE Now r16 is 0 because of the loop above, we use that.
	st Y+, r16
	st Y+, r16
	st Y+, r24 ; FNORDER FN_LEN must be here.
	st Y+, r23 ; r24-r21 are file size.
	st Y+, r22
	st Y+, r21
	sbiw r29:r28, FN_SIZE
	mov r27, r31 ; Store old Z in X.
	mov r26, r30
	mov r31, r29 ; NOTE We move it to Z because other functions use that.
	mov r30, r28
	clc
	ret
FAT_open_seek:
	; Seek forward one sector.
	ldi r17, 2
	clr r16
	call FAT_seek_dir
	brcs FAT_open_fail
	; Check whether this was the last cluster.
	adiw r31:r30, FN_CCL
	ld r16, Z
	sbiw r31:r30, FN_CCL
	andi r16, 0x80
	brne FAT_open_notfound
	; Continue searching.
	rjmp FAT_open_searchloop
FAT_open_notfound:
	ldi r16, ERR_FNF
FAT_open_fail:
	pop r26 ; Target file handle restored.
	pop r27
	sec
	ret

;
; FAT_read
;
;  Read bytes from a file and advance the pointer.
;
;  Input:
;    Y (r29:r28): Data destination.
;    Z (r31:r30): File handle.
;    r23:r22: Length of data to read.
;
;  Output:
;    Y (SRAM): End of data read (if success). This can tell how much data was read.
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;
;  Notes:
;    Seeking a distance of 0, will give ERR_EOF.
;    This doesn't work on directories. They will act as 0-length file.
;
FAT_read:
	; Make sure we're not reading beyond EOF.
	call FAT_seekmax ; Truncates to max amount we could read.
	clr r16
	cp r23, r16
	cpc r22, r16
	brne +3 ; If it's 0 this is the end.
		ldi r16, ERR_EOF
		sec
		ret
	; Store length for later.
	push r23
	push r22
FAT_read_continue:
	; Load a sector.
	call FAT_read_sector ; This leaves r22, r23 alone, but does trash X.
	brcs FAT_read_fail
	; Find position in sector.
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
	adiw r31:r30, FN_POS + 2
	ld r17, Z+ ; Lower 2 bytes of FN_POS.
	ld r16, Z+
	sbiw r31:r30, FN_POS + 4 ; Restore Z.
	andi r17, 1 ; & 0x1FF gives offset in sector.
	add r26, r16
	adc r27, r17
FAT_read_byte:
	; Copy a byte from SD_BUF to DST.
	ld r16, X+
	st Y+, r16
	; Decrement r23:r22 (len) and loop if nonzero.
	clr r16
	subi r22, 1
	sbc r23, r16
	cpi r23, 0
	brne +2
		cpi r22, 0
		breq FAT_read_done
	; Check if we're past the sector boundary.
	cpi r27, high(SD_BUF + 512)
	brne +2
		cpi r26, low(SD_BUF + 512)
		breq FAT_read_nextsector
	rjmp FAT_read_byte
FAT_read_done:
	; Seek to new FN_POS and return.
	pop r16
	pop r17
	call FAT_seek
	ret ; Error from FAT_seek will just be passed on.
FAT_read_nextsector:
	; Get 512 - FN_POS % 512.
	adiw r31:r30, FN_POS + 2
	ld r19, Z+ ; Lower 2 bytes of FN_POS.
	ld r18, Z+
	sbiw r31:r30, FN_POS + 4 ; Restore Z.
	andi r19, 1 ; & 0x1FF gives offset in sector.
	ldi r17, 2 ; Load 512.
	clr r16
	sub r16, r18 ; Subtract that shit.
	sbc r17, r19
	; Subtract it from stored length.
	pop r18
	pop r19
	sub r18, r16
	sbc r19, r17
	push r19
	push r18
	; Seek, restore our shit, and resume operation.
	push r23 ; Save r23:r22, our length.
	push r22 ; FAT_seek could trash it.
	call FAT_seek
	pop r22
	pop r23
	brcs FAT_read_fail ; Error check for FAT_seek.
	rjmp FAT_read_continue
FAT_read_fail:
	pop r22
	pop r23
	ret

;
; FAT_write
;
;  Write bytes to a file and advance the pointer.
;
;  Input:
;    Y (r29:r28): Data source.
;    Z (r31:r30): File handle.
;    r23:r22: Length of data to write.
;
;  Output:
;    Y (SRAM): End of data written (if success).
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;
FAT_write:
	; Make sure there's a cluster.
	push r22
	call FAT_firstcluster ; Preserve r23.
	pop r22
	brcc +1
		ret
	; Store length for later.
	push r23
	push r22
FAT_write_continue:
	; Do make sure we actually have a cluster to load.
	call FAT_wantcluster ; Preserves r23:r22.
	brcs FAT_write_fail
	; Load a sector.
	call FAT_read_sector ; This leaves r22, r23 alone, but does trash X.
	brcs FAT_write_fail ; If FAT_EOF we'll simply create a cluster first.
	; Find position in sector.
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
	adiw r31:r30, FN_POS + 2
	ld r17, Z+ ; Lower 2 bytes of FN_POS.
	ld r16, Z+
	sbiw r31:r30, FN_POS + 4 ; Restore Z.
	andi r17, 1 ; & 0x1FF gives offset in sector.
	add r26, r16
	adc r27, r17
FAT_write_byte:
	; Copy a byte from SRC to SD_BUF.
	ld r16, Y+
	st X+, r16
	; Decrement r23:r22 (len) and loop if nonzero.
	clr r16
	subi r22, 1
	sbc r23, r16
	cpi r23, 0
	brne +2
		cpi r22, 0
		breq FAT_write_done
	; Check if we're past the sector boundary.
	cpi r27, high(SD_BUF + 512)
	brne +2
		cpi r26, low(SD_BUF + 512)
		breq FAT_write_nextsector
	rjmp FAT_write_byte
FAT_write_done:
	; Write out what we've copied.
	call FAT_write_sector
	brcs FAT_write_fail
	; Seek to new FN_POS and return.
	pop r16
	pop r17
	call FAT_seek
	ret ; Error from FAT_seek will just be passed on.
FAT_write_nextsector:
	; Write out the previous sector.
	call FAT_write_sector
	brcs FAT_write_fail
	; Get 512 - FN_POS % 512.
	adiw r31:r30, FN_POS + 2
	ld r19, Z+ ; Lower 2 bytes of FN_POS.
	ld r18, Z+
	sbiw r31:r30, FN_POS + 4 ; Restore Z.
	andi r19, 1 ; & 0x1FF gives offset in sector.
	ldi r17, 2 ; Load 512.
	clr r16
	sub r16, r18 ; Subtract that shit.
	sbc r17, r19
	; Subtract it from stored length.
	pop r18
	pop r19
	sub r18, r16
	sbc r19, r17
	push r19
	push r18
	; Seek, restore our shit, and resume operation.
	push r23 ; Save r23:r22, our length.
	push r22 ; FAT_seek could trash it.
	call FAT_seek
	pop r22
	pop r23
	brcs FAT_write_fail ; Error check for FAT_seek.
	rjmp FAT_write_continue
FAT_write_fail:
	pop r22
	pop r23
	sec
	ret

;
; FAT_getlength
;
;  Get length of a file.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    r20:r19:r18:r17: File length.
;
;  Side effects:
;    -
;
FAT_getlength:
	adiw r31:r30, FN_LEN
	ld r20, Z+
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	sbiw r31:r30, FN_LEN + 4
	ret

; TODO FAT_isdir, etc, tools to check whether files exist and are directories or files.

;
; FAT_creat
;
;  Create a new file (or partial directory).
;
;  Input:
;    Y (r29:r28): File name (11 bytes) + attributes (1 byte) + time (2 bytes) + date (2 bytes).
;    Z (r31:r30): Directory file handle.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;    T flag too.
;
;  Notes:
;    - This will create duplicate files if one already exists, be careful.
;    - Y is preserved so that one can open the file easily after creation.
;    - In a new file we don't need clusters yet, just set first cluster = 0.
;    - It's a requirement for a folder to have a cluster though.
;
FAT_creat:
	call FAT_seek_start
	; Find and load sector of folder.
FAT_creat_sector:
	; Read a sector.
	call FAT_read_sector
	brcc +1
		ret
	; Attempt to find dir entry.
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
FAT_creat_loop:
	ld r16, X
	cpi r16, 0
	breq FAT_creat_gotent
	cpi r16, 0xE5
	breq FAT_creat_gotent
	adiw r27:r26, 32 ; Size of a directory entry.
	; Try again if this ain't the end.
	cpi r26, low(SD_BUF + 512)
	brne FAT_creat_loop
	cpi r27, high(SD_BUF + 512)
	brne FAT_creat_loop
	; No dice, read next sector and retry.
	ldi r17, 2 ; seek(512)
	clr r16
	call FAT_seek_dir
	brcs FAT_creat_end
	; We make sure a sector exists if none is there yet.
	adiw r31:r30, FN_CCL
	ld r16, Z
	sbiw r31:r30, FN_CCL
	andi r16, 0x80
	breq FAT_creat_gotcl
	; Reaching here means we need a new cluster.
	call FAT_wantcluster
	brcs FAT_creat_end
	; The cluster should be filled with zeros.
	call FAT_clearsdbuf
	; Now write this to every sector of the cluster.
	lds r23, FAT_SPC
	clr r22
FAT_create_writenewloop:
	call FAT_find_sector ; Preserve r22, r23.
	clr r16
	add r17, r22
	adc r18, r16
	adc r19, r16
	adc r20, r16
	call SD_write ; Preserve r22, r23.
	inc r22
	cp r22, r23
	brne FAT_create_writenewloop
FAT_creat_gotcl:
	rjmp FAT_creat_sector
FAT_creat_gotent:
	; We found the last dir entry, overwrite it with the new file.
	ldi r16, 12 ; Filename + attr.
		ld r17, Y+
		st X+, r17
		dec r16
	brne -4
	clr r18
	ldi r16, 10 ; Reserved bytes + high byte of FCL.
		st X+, r18
		dec r16
	brne -3
	ldi r16, 4 ; Time + date.
		ld r17, Y+
		st X+, r17
		dec r16
	brne -4
	ldi r16, 6 ; Low word of cluster + file size.
		st X+, r18 ; r18 was already 0
		dec r16
	brne -3
	; Restore Y.
	sbiw r29:r28, 16
	; Write sector.
	call FAT_write_sector
FAT_creat_end:
	ret ; Pass error onwards.

;
; FAT_mkdir
;
;  Create a new directory and open it.
;
;  Input:
;    X (r27:r26): Space allocated for new directory.
;    Y (r29:r28): File name (11 bytes) + attributes (1 byte) + time (2 bytes) + date (2 bytes).
;    Z (r31:r30): Directory file handle (parent).
;
;  Output:
;    Z (r31:r30): Directory file handle (created).
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;    T flag too.
;
;  Notes:
;    - This will create duplicate files/directories if one already exists, be careful.
;    - User must make sure bit 4 (or with 0x10) of attributes is set (directory flag).
;
FAT_mkdir:
	push r27
	push r26
	call FAT_creat
	pop r26
	pop r27
	brcc +1
	rjmp FAT_mkdir_end
	call FAT_open ; This will move Z to X.
	brcc +1
	rjmp FAT_mkdir_end
	mov r29, r27 ; Move Z to Y. We won't touch it for a while.
	mov r28, r26
	call FAT_firstcluster
	brcc +1
	rjmp FAT_mkdir_end
	; Fill . and .. nodes, zero pad the whole cluster.
	call FAT_clearsdbuf
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
	; Write "." dir entry.
	ldi r16, 0x2E ; File name
	st X+, r16
	ldi r16, 0x20
	ldi r17, 10
		st X+, r16
		dec r17
	brne -3
	ldi r16, 0x10
	st X+, r16
	adiw r27:r26, 8 ; "Reserved"
	adiw r31:r30, FN_FCL + 2
	ld r16, -Z ; High word of cluster.
	st X+, r16
	ld r16, -Z
	st X+, r16
	adiw r27:r26, 4 ; Date and time. NOTE Should we copy from folder?
	adiw r31:r30, 4
	ld r16, -Z ; Low word of cluster.
	st X+, r16
	ld r16, -Z
	st X+, r16
	sbiw r31:r30, FN_FCL + 2 ; Restore Z.
	adiw r27:r26, 4 ; Size is 0.
	; Write ".." dir entry.
	ldi r16, 0x2E ; File name
	st X+, r16
	st X+, r16
	ldi r16, 0x20
	ldi r17, 9
		st X+, r16
		dec r17
	brne -3
	ldi r16, 0x10
	st X+, r16
	; If Y == FAT_RFN we actually want the cluster to be 0.
	cpi r29, high(FAT_RFN)
	brne FAT_mkdir_notroot
	cpi r28, low(FAT_RFN)
	brne FAT_mkdir_notroot
	; NOTE X at reserved part of "..".
	rjmp FAT_mkdir_wasroot
FAT_mkdir_notroot:
	adiw r27:r26, 8 ; "Reserved"
	adiw r29:r28, FN_FCL + 2
	ld r16, -Y ; High word of cluster.
	st X+, r16
	ld r16, -Y
	st X+, r16
	adiw r27:r26, 4 ; Date and time.
	adiw r29:r28, 4
	ld r16, -Y ; Low word of cluster.
	st X+, r16
	ld r16, -Y
	st X+, r16
	sbiw r29:r28, FN_FCL + 2 ; Restore Y.
	; NOTE X at file size of "..", different of as it was root folder.
FAT_mkdir_wasroot:
	; Now write this to every sector of the cluster.
	lds r23, FAT_SPC
	clr r22
FAT_mkdir_writenewloop:
	call FAT_find_sector ; Preserve r22, r23.
	clr r16
	add r17, r22
	adc r18, r16
	adc r19, r16
	adc r20, r16
	call SD_write ; Preserve r22, r23.
	brcs FAT_mkdir_end
	cpi r22, 0 ; Clear SD_BUF again after writing first sector.
	brne +1 ; NOTE this is not very optimal, we could do faster.
	call FAT_clearsdbuf
	inc r22
	cp r22, r23
	brne FAT_mkdir_writenewloop
	; The end.
	clc
FAT_mkdir_end:
	ret

;
; FAT_unlink
;
;  Delete a file.
;
;  Input:
;    Z (r31:r30): File node, file needs to be open to be deleted.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r2 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;    T flag too.
;
;  Notes:
;    Do not use this on directories.
;
FAT_unlink:
	; Get the first cluster.
	adiw r31:r30, FN_FCL
	ld r19, Z+ ; See FAT_killcluster and FAT_getclsector for why this is awkward.
	ld r18, Z+
	ld r17, Z+
	ld r20, Z+
	sbiw r31:r30, FN_FCL + 4 ; Restore Z.
	clt ; Clear T, as we don't want to write EOF.
	; Check if we actually need to do anything.
	clr r21
	cp r20, r21
	cpc r17, r21
	cpc r18, r21
	cpc r19, r21 ; No clusters means already at length 0.
	brne +1
	rjmp FAT_unlink_continue ; NOTE not error.
	; Murder the clusters.
FAT_unlink_loop:
	call FAT_killcluster
	brcc +1
		rjmp FAT_unlink_fail
	; Check if the next one is the last one.
	ldi r16, 0xFF
	cp r20, r16
	cpc r17, r16
	cpc r18, r16
	ldi r16, 0x0F
	cpc r19, r16
	breq FAT_unlink_end ; NOTE this could loop endlessly on broken filesystem.
	rjmp FAT_unlink_loop
FAT_unlink_end:
	; Kill the last cluster.
	call FAT_killcluster
	brcs FAT_unlink_fail
FAT_unlink_continue:
	; Write dir entry.
	call FAT_getdent
	brcs FAT_unlink_fail
	ldi r16, 0xE5 ; This means "deleted file".
	st X+, r16
	ld r20, Z+ ; FNORDER Want FN_DFSE.
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	sbiw r31:r30, FN_DFSE + 4
	call SD_write
FAT_unlink_fail:
	ret ; SD_write error fall through.

;
; FAT_trunc
;
;  Shrink the size of a file to current position.
;
;  Input:
;    Z (r31:r30): File node.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r2 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;    T flag too.
;
;  Notes:
;    Do not use this on directories, as it will write file size.
;
FAT_trunc:
	; Clear T if the file should be made empty.
	set ; This tells FAT_killcluster to write last sector mark.
	adiw r31:r30, FN_POS
	clr r18 ; NOTE This whole thing is quite suboptimal.
	ld r17, Z+ ; We look at FN_POS later too, this could improve.
	or r18, r17
	ld r17, Z+
	or r18, r17
	ld r17, Z+
	or r18, r17
	ld r17, Z+
	or r18, r17
	brne +1
	clt
	bld r2, 0 ; Store T for later.
	; Read the current cluster.
	sbiw r31:r30, FN_POS + 4 - FN_CCL ; FNORDER Want FN_CCL.
	ld r19, Z+ ; See FAT_killcluster and FAT_getclsector for why this is awkward.
	ld r18, Z+
	ld r17, Z+
	ld r20, Z+
	andi r19, 0xF0
	sbiw r31:r30, FN_CCL + 4 ; Restore Z.
	; Check if we actually need to do anything.
	clr r21
	cp r20, r21
	cpc r17, r21
	cpc r18, r21
	cpc r19, r21 ; No clusters means already at length 0.
	clc
	brne +1
	rjmp FAT_trunc_fail ; NOTE not error.
	; Murder the clusters.
FAT_trunc_loop:
	call FAT_killcluster
	brcc +1
		rjmp FAT_trunc_fail
	; Check if the next one is the last one.
	ldi r16, 0xFF
	cp r20, r16
	cpc r17, r16
	cpc r18, r16
	ldi r16, 0x0F
	cpc r19, r16
	clt ; This tells FAT_killcluster to free a sector.
	breq FAT_trunc_end ; NOTE this could loop endlessly on broken filesystem.
	rjmp FAT_trunc_loop
FAT_trunc_end:
	; Kill the last cluster.
	call FAT_killcluster
	brcs FAT_trunc_fail
	; Copy FN_POS to FN_LEN.
	adiw r31:r30, FN_POS
	ld r20, Z+
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	st Z+, r20 ; FNORDER FN_LEN sequential with FN_POS.
	st Z+, r19
	st Z+, r18
	st Z+, r17
	sbiw r31:r30, FN_POS + 8 ; FNORDER
	; If the file is empty, clear FCL and CCL.
	bst r2, 0
	brts FAT_trunc_nonempty
	; Reaching here means the file has 0-length and we should clear the first sector.
	adiw r31:r30, FN_FCL
	clr r16
	st Z+, r16
	st Z+, r16
	st Z+, r16
	st Z+, r16
	st Z+, r16 ; FNORDER FN_CCL after FN_FCL.
	st Z+, r16
	st Z+, r16
	st Z+, r16
	sbiw r31:r30, FN_FCL + 8 ; FNORDER
FAT_trunc_nonempty:
	; Write dir entry (unless this is a folder).
	call FAT_getdent
	brcs FAT_trunc_fail
	; Write out FN_FCL, it may or may not be changed.
	adiw r31:r30, FN_FCL
	ld r20, Z+
	andi r20, 0xF0
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	adiw r27:r26, 0x14 ; FCL high word.
	st X+, r19
	st X+, r20
	adiw r27:r26, 4 ; FCL low word, file size follows this.
	st X+, r17
	st X+, r18
	; Write out FN_LEN.
	adiw r31:r30, FN_LEN + 4 - (FN_FCL + 4) ; FNORDER
	ld r16, -Z ; Reverse the byte order.
	st X+, r16
	ld r16, -Z
	st X+, r16
	ld r16, -Z
	st X+, r16
	ld r16, -Z
	st X+, r16
	sbiw r31:r30, FN_LEN
	ld r20, Z+ ; FNORDER Want FN_DFSE.
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	sbiw r31:r30, FN_DFSE + 4
	call SD_write
FAT_trunc_fail:
	ret ; SD_write error fall through.

;
; FAT_seek, FAT_seek_dir
;
;  Seek forward in file, also grows files if seeked beyond the end.
;  FAT_seek_dir is for directories, as those want 0 file size forever.
;
;  Input:
;    Z (r31:r30): File handle.
;    r17:r16: How far to seek forward.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;    T flag trashed.
;
;  Notes:
;    If we seek to right at a cluster boundary, a new cluster is not created.
;      That way we can see to the end of a cluster without enlarging the file.
;
FAT_seek_dir:
	set
	rjmp FAT_seek_real
FAT_seek:
	clt
FAT_seek_real:
	; If seeking 0, we got nothing to do and it could fuck with our math.
	clr r18
	cp r16, r18
	cpc r17, r18
	brne +2
		clc ; Not error.
		ret
	; Make sure there's a cluster.
	push r17 ; This is also why we run away from 0 argument.
	push r16
	call FAT_firstcluster
	pop r16
	pop r17
	brcc +1
		ret
	; Get FN_POS.
	adiw r31:r30, FN_POS
	ld r22, Z+
	ld r21, Z+
	ld r20, Z+
	ld r19, Z+
	; We'll need the lower bits to tell whether we crossed a cluster boundary.
	mov r24, r20
	mov r23, r19
	; Add the offset and store back to FN_POS.
	add r19, r16
	adc r20, r17
	clr r18
	adc r21, r18
	adc r22, r18
	; Store FN_POS.
	st -Z, r19 ; FNORDER End of FN_POS.
	st -Z, r20
	st -Z, r21
	st -Z, r22
	; Check if we're past EOF, if so grow file.
	adiw r31:r30, FN_LEN + 4 - FN_POS ; FNORDER Go to end of FN_LEN
	ld r18, -Z ; Low byte of FN_LEN.
	cp r19, r18
	ld r18, -Z ; N.
	cpc r20, r18
	ld r18, -Z
	cpc r21, r18
	ld r18, -Z
	cpc r22, r18
	brcs FAT_seek_noteof ; If LEN > POS, carry is set.
	rcall FAT_seek_eof ; Must preserve r24:r23 and r17:r16.
	brcc +3
		sbiw r31:r30, FN_LEN ; Restore Z.
		sec
		ret
FAT_seek_noteof:
	sbiw r31:r30, FN_LEN ; Restore Z, we don't need it for now.
	; Find the amount of clusters to move forward.
	; ((  fn_pos % (FAT_SPC * 512)  ) + offset) / 512 / FAT_SPC
	;  | Bytes past current cluster |   ^ Bytes added.
	; We stored for this lower bytes of old pos in r24:r23.
	; And kept the argument in r17:r16.
	lds r18, FAT_SPC
	dec r18 ; We make a bit mask that boils down to "% FAT_SPC * 512".
	sec ; Lowest bit of r24 is about sectors still.
	rol r18 ; So we roll in 1 bit.
	and r24, r18 ; Note that we depend on the fact FAT_SPC has only 1 bit set.
	add r23, r16 ; + offset
	adc r24, r17 ; NOTE FAT_seek_eof must preserve r17:r16.
	lds r18, FAT_SPC
		lsr r24 ; We shift once, then we have amount of sectors moved forward.
		lsr r18
	brne -3 ; Then we shift once more for every bit we can shift FAT_SPC.
	; Skip r24 clusters.
	inc r24
FAT_seek_nextcluster:
	dec r24
	breq FAT_seek_foundcluster
	; If (r24 == 1 && cluster_aligned && nextisEOF)
	;   FN_CCL &= 0x80000000
	;   rjmp FAT_seek_foundcluster
	cpi r24, 1 ; If r24 != 1
	brne FAT_seek_nextcluster_continue
	; Check if cluster-aligned.
	adiw r31:r30, FN_POS + 2
	ld r20, Z+
	ld r19, Z+
	sbiw r31:r30, FN_POS + 4
	lds r18, FAT_SPC
	dec r18 ; We make a bit mask that boils down to "% FAT_SPC * 512"
	sec ; Lowest bit of r24 is about sectors still.
	rol r18 ; So we roll in 1 bit.
	and r20, r18 ; Note that we depend on the fact FAT_SPC has only 1 bit set.
	cpi r20, 0
	brne FAT_seek_nextcluster_continue
	cpi r19, 0
	brne FAT_seek_nextcluster_continue
	; Check if next is EOF.
	push r24
	call FAT_getnextcluster
	pop r24
	brcc +1
		ret
	ori r20, 0xF0 ; They oughta call it fat28. We or cause we want EOF = 0xFFFFFFFF.
	ldi r16, 0xFF
	cp r17, r16
	cpc r18, r16
	cpc r19, r16
	cpc r20, r16
	brne FAT_seek_nextcluster_continue
	; Tell rest of the world the next cluster doesn't exist yet.
	adiw r31:r30, FN_CCL
	ld r16, Z+
	ori r16, 1 << 7	; If the CCL is not yet created we set it's 31th bit.
	st -Z, r16
	sbiw r31:r30, FN_CCL
	rjmp FAT_seek_foundcluster
FAT_seek_nextcluster_continue:
	push r24
	call FAT_nextcluster
	pop r24
	brcc +1
		ret
	rjmp FAT_seek_nextcluster
FAT_seek_foundcluster:
	; Returne withe da successe.
	clc
	ret
FAT_seek_eof: ; FNORDER When this gets called FN_LEN is at Z and (new) FN_POS is in r22:r21:r20:r19.
	push r24 ; We must preserve r24:r23 and r17:r16. r23 isn't touched by anthing here.
	push r17
	push r16
	; We copy FN_POS to FN_LEN since we're beyond EOF.
	st Z+, r22
	st Z+, r21
	st Z+, r20
	st Z+, r19
	sbiw r31:r30, FN_LEN + 4 ; Go back to start of file handle. FNORDER we want FN_DFSE.
	; Check if this is the FAT_seek_dir.
	clc ; This ain't an error.
	brts FAT_seek_eof_fail
	; Store new file length to file handle.
	ld r20, Z+
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	call SD_read
	brcc +2
		adiw r31:r30, FN_LEN - 4 ; FNORDER
		rjmp FAT_seek_eof_fail
	ldi r27, high(SD_BUF + 0x1C) ; File size is at 0x1C in dir entry.
	ldi r26, low(SD_BUF + 0x1C)
	ld r17, Z+ ; FNORDER We expect FN_DOFF.
	ld r16, Z+
	add r26, r16
	adc r27, r17
	adiw r31:r30, FN_LEN + 4 - (FN_DOFF + 2) ; FNORDER Now we want end of FN_LEN.
	ld r16, -Z ; We copy and change endianness.
	st X+, r16
	ld r16, -Z
	st X+, r16
	ld r16, -Z
	st X+, r16
	ld r16, -Z
	st X+, r16
	sbiw r31:r30, FN_LEN ; FNORDER We want FN_DFSE again.
	ld r20, Z+
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	call SD_write ; Don't touch carry after this until ret.
	brcc +3
		adiw r31:r30, FN_LEN - 4 ; FNORDER Z must be at FN_LEN when we return.
		sec ; adiw trashes carry.
		ret
	adiw r31:r30, FN_LEN - 4 ; FNORDER Z must be at FN_LEN when we return.
	clc
FAT_seek_eof_fail:
	pop r16
	pop r17
	pop r24
	ret ; FAT_nextcluster will take care of adding enough clusters.

;
; FAT_seek_start
;
;  Seeks to start of file.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    -
;
;  Side effects:
;    r16, r17, r18, r19 trashed.
;
FAT_seek_start:
	; Get first cluster and copy to current cluster.
	adiw r31:r30, FN_FCL
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	ld r16, Z+
	st Z+, r19 ; FNORDER
	st Z+, r18
	st Z+, r17
	st Z+, r16
	; Set FN_POS to 0.
	clr r16
	st Z+, r16 ; FNORDER
	st Z+, r16
	st Z+, r16
	st Z+, r16
	sbiw r31:r30, 12 + FN_FCL ; FNORDER
	ret

;
; FAT_seek_end
;
;  Seeks to end of file.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;
;  Notes:
;    This doesn't work on directories.
;
FAT_seek_end:
	; Find how far we can seek.
	ldi r23, 0xFF
	ldi r22, 0xFF
	call FAT_seekmax
	; If that's 0 we quit.
	clr r16
	cp r22, r16
	cpc r23, r16
	brne +1
		ret
	; Seek that far and restart.
	mov r16, r22
	mov r17, r23
	call FAT_seek
	rjmp FAT_seek_end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Below functions are limited use, for optimizations and low level trickery. ;;
;; Also they're important for the rest of the library                         ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; FAT_read_sector
;
;  Reads current sector of a file. ERR_EOF returned on 0 length file.
;
;  Input:
;    Z (r31:r30): File handle.
;      Sector # is rounded down if FN_POS is misaligned.
;
;  Output:
;    SD_BUF (SRAM): The sector's contents.
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r24, r25, r27:r26 (X) trashed.
;
;  Notes:
;    This would be faster than FAT_read.
;
FAT_read_sector:
	call FAT_find_sector
	brcs +1
		call SD_read
	ret ; SD_read error passes through.

;
; FAT_write_sector
;
;  Writes current sector of a file.
;
;  Input:
;    Z (r31:r30): File handle.
;      Sector # is rounded down if FN_POS is misaligned.
;    SD_BUF (SRAM): What to write to the sector.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r24, r25, r27:r26 (X) trashed.
;
;  Notes:
;    This would be faster than FAT_write.
;    For 0-length files this throws ERR_EOF, but that could be easily changed.
;      See FAT_firstcluster. Files with 0 length have first cluster = 0.
;
FAT_write_sector:
.if 0 ; Uncomment for create cluster on empty file.
	call FAT_firstcluster ; NOTE Does trash r22, check code and update description before uncommenting.
	brcc +1
		ret
.endif
	call FAT_find_sector
	call SD_write
	ret

;
; FAT_seekmax
;
;  Find out whether FN_POS + r23:r22 is more than FN_LEN.
;  If so r23:r22 is loaded with FN_LEN - FN_POS.
;  I.E. Find the max distance we can seek forward without crossing EOF.
;
;  Input:
;    Z (r31:r30): File handle.
;    r23:r22: The number to work with.
;
;  Output:
;    r23:r22: Distance to EOF or original value, whichever is smallest.
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21 trashed.
;
;  Notes:
;    This doesn't work on directories. For directories result will be 0-length
;
FAT_seekmax:
	; Load FN_LEN.
	adiw r31:r30, FN_LEN
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	ld r16, Z+
	sbiw r31:r30, 4 ; FNORDER We want end of FN_POS now.
	; Subtract FN_POS.
	ld r20, -Z
	sub r16, r20 ; Which is FN_LEN - FN_POS.
	ld r20, -Z
	sbc r17, r20
	ld r20, -Z
	sbc r18, r20
	ld r20, -Z
	sbc r19, r20
	sbiw r31:r30, FN_POS ; Restore Z.
	; FNORDER Z is now FN_POS, r19:r18:r17:r16 is now FN_LEN - FN_POS
	; Check if r23:r22 is bigger.
	clr r20
	cp r16, r22
	cpc r17, r23
	cpc r18, r20
	cpc r19, r20
	brcc FAT_seekmax_nochange
	; r23:r22 was bigger, shrink it.
	mov r22, r16
	mov r23, r17
	ret
FAT_seekmax_nochange:
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Functions below this are only intended for use by the driver itself. ;;
;; They probably won't be very useful for anything else.                ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; FAT_find_sector
;
;  Find current sector of a file.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    r20:r19:r18:r17: The sector index, ready for SD_read/write.
;    C flag set on error, r16 loaded with ERR_EOF, this for empty file only.
;
;  Side effects:
;    r16, r21 trashed.
;
FAT_find_sector:
	; Load the current cluster.
	adiw r31:r30, FN_CCL
	ld r20, Z+
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+ ; We depend on Z value later.
	; Check if the CCL is one waiting to be created.
	andi r20, 0x80
	brne FAT_find_sector_eof
	;andi r20, 0xF0 ; NOTE if we'd use other bits we should do this.
	; If there is no cluster, ERR_EOF.
	clr r16
	cp r17, r16
	cpc r18, r16
	cpc r19, r16
	cpc r20, r16
	brne +4
FAT_find_sector_eof:
		sbiw r31:r30, FN_CCL + 4
		ldi r16, ERR_EOF
		sec
		ret
	; Subtract 2.
	subi r17, 2 ; First cluster is 2.
	sbc r18, r16
	sbc r19, r16
	sbc r20, r16
	; Multiply by the sectors/cluster.
	lds r16, FAT_SPC
FAT_find_sector_shift:
	lsr r16
	breq FAT_find_sector_endshift
	lsl r17
	rol r18
	rol r19
	rol r20
	rjmp FAT_find_sector_shift
FAT_find_sector_endshift:
	; Add (FN_POS / 512) % FAT_SPC.
	adiw r31:r30, FN_POS - (FN_CCL + 4) + 2 ; FNORDER
	ld r21, Z
	sbiw r31:r30, FN_POS + 2 ; Restore Z cause we don't need it anymore.
	lsr r21 ; r21 is now: (FN_POS >> 9) & 0x7F
	lds r16, FAT_SPC ; Note that we depend on FAT_SPC only having 1 bit set.
	dec r16 ; So top bit never set now.
	and r21, r16 ; r21 % FAT_SPC
	clr r16
	add r17, r21
	adc r18, r16
	adc r19, r16
	adc r20, r16
	; Add FAT_FCL to this.
	lds r16, FAT_FCL + 3 ; LSB
	add r17, r16
	lds r16, FAT_FCL + 2
	adc r18, r16
	lds r16, FAT_FCL + 1
	adc r19, r16
	lds r16, FAT_FCL + 0 ; MSB
	adc r20, r16
	clc
	ret

;
; FAT_getdent
;
;  Load the appropriate sector of containing directory, and put X at dir entry in SD_BUF.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;    r27:r26 (X): Points to dir entry in SD_BUF.
;    SD_BUF (SRAM): Appropriate sector of dir entry.
;
;  Side effects:
;    r16, r17, r18, r19, r20, r24, r25, SD_BUF (SRAM) trashed.
;
FAT_getdent:
	ld r20, Z+ ; FNORDER FN_DFSE at start.
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	call SD_read
	brcc +3
		sbiw r31:r30, FN_DFSE + 4
		sec
		ret
	ld r17, Z+ ; FNORDER FN_DOFF after DFSE.
	ld r16, Z+
	ldi r27, high(SD_BUF) ; SD_BUF + FN_DOFF = direntry.
	ldi r26, low(SD_BUF)
	add r26, r16
	adc r27, r17
	sbiw r31:r30, FN_DFSE + 6 ; Restore Z.
	clc
	ret

;
; FAT_getnextcluster
;
;  Read cluster pointer.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;    r20:r19:r18:r17: The next cluster points to...
;
;  Side effects:
;    r0 (only bit 0), r16, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;
FAT_getnextcluster:
	; Pointer to the next cluster is in sector: FAT_AFAT + (FN_CCL * 4 / 512).
	;   Which equates to (thiscluster >> 7) + (FAT_FLAGS & 0x0F) * FAT_SPF
	adiw r31:r30, FN_CCL
	call FAT_getclsector ; Get cluster of CCL pointer.
	sbiw r31:r30, FN_CCL
	call FAT_addafat ; Add FAT_AFAT.
	call SD_read ; Preserve r22.
	brcc +1 ; Too far.
		ret ; Flags set by SD_read.
	; Now we have the sector that holds the pointer.
	call FAT_getclsector_load
	ld r17, X+ ; It's little endian that's why this is backwards.
	ld r18, X+
	ld r19, X+
	ld r20, X+
	ret

;
; FAT_nextcluster
;
;  Advance FN_CCL by dancing through the FAT.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;    r20:r19:r18:r17: The new FN_CCL.
;    FN_CCL of file handle also updated.
;
;  Side effects:
;    r0 (only bit 0), r16, r21, r22, r23, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;
FAT_nextcluster:
	; Pointer to the next cluster is in sector: FAT_AFAT + (FN_CCL * 4 / 512).
	;   Which equates to (thiscluster >> 7) + (FAT_FLAGS & 0x0F) * FAT_SPF
	adiw r31:r30, FN_CCL ; We pretty much keep Z at FN_CCL the whole function.
FAT_nextcluster_retry:
	; NOTE This is very similar to FAT_getnextcluster, we could reuse.
	call FAT_getclsector ; Get cluster of CCL pointer.
	call FAT_addafat ; Add FAT_AFAT.
	call SD_read ; Preserve r22.
	brcc +1 ; Too far.
		rjmp FAT_nextcluster_fail ; Flags set by SD_read.
	; Now we have the sector that holds the pointer.
	call FAT_getclsector_load
	ld r17, X+ ; It's little endian that's why this is backwards.
	ld r18, X+
	ld r19, X+
	ld r20, X+
	; Check if it's EOF.
	ori r20, 0xF0 ; They oughta call it fat28. We or cause we want EOF = 0xFFFFFFFF.
	ldi r16, 0xFF
	cp r17, r16
	cpc r18, r16
	cpc r19, r16
	cpc r20, r16
	brne FAT_nextcluster_exists
	call FAT_getfree ; NOTE After this preserve r21 until FAT_getfree_load.
	brcs FAT_nextcluster_fail
	; Read FAT where CCL is.
	call FAT_getclsector ; Get cluster of CCL pointer. Preserve r22 too now!
	call FAT_addafat ; Add FAT_AFAT. Preserve r11, r22.
	call SD_read ; Preserve r21, r22.
	brcs FAT_nextcluster_fail
	call FAT_getclsector_load ; Preserve r21.
	; X now holds pointer to CCL entry in SD_BUF.
	call FAT_getfree_load	
	; Now r20:..:r17 holds the new cluster, X points to CCL for the file FAT sector.
	st X+, r17 ; Little endian.
	st X+, r18
	st X+, r19
	st X+, r20
	; SD_BUF has been updated to hold a new sector for the FAT.
	call FAT_getclsector
	call FAT_tablewrite ; With the help of this of course.
	brcs FAT_nextcluster_fail
	; NOTE On a broken system this can become an endless loop.
	rjmp FAT_nextcluster_retry ; We restart ourselves, it should update the file handle now.
FAT_nextcluster_exists:
	; Store the next cluster to FN_CCL, which Z is at the end of.
	andi r20, 0x0F ; Make it sane again first.
	st Z+, r20
	st Z+, r19
	st Z+, r18
	st Z+, r17
	sbiw r31:r30, FN_CCL + 4
	clc
	ret
FAT_nextcluster_fail:
	sbiw r31:r30, FN_CCL
	sec ; sbiw affects carry.
	ret

;
; FAT_getclsector, FAT_getclsector2
;
;  Get the FAT sector # that holds the pointer for next cluster.
;  FAT_clgetsector2 is same but doesn't load from Z itself.
;    To load Z for it is convoluted, read the function.
;
;  Input:
;    Z (r31:r30): Point to a cluster, i.e. file handle + FN_FCL or FN_CCL.
;
;  Output:
;    r20:r19:r18:r17: The sector # (without FAT_START or FAT_AFAT).
;    r22: Low byte of cluster, so you can find it in the sector.
;
;  Side effects:
;    -
;
;  Notes:
;    See: FAT_getclsector_load, FAT_addafat
;
FAT_getclsector:
	; Pointer to the next cluster is in sector: FAT_AFAT + (FN_CCL * 4 / 512).
	;   Which equates to (cluster >> 7) + (FAT_FLAGS & 0x0F) * FAT_SPF
	ld r19, Z+ ; We load it shifted 8 bytes right in r19:r18:r17.
	ld r18, Z+
	ld r17, Z+
	ld r20, Z+ ; r20 holds the LSB for now.
	sbiw r31:r30, 4 ; Restore Z.
FAT_getclsector2:
	andi r19, 0xF0 ; We use the higher bits for info.
	mov r22, r20 ; r22 is preserved by SD_read, we want the LS byte of CCL later.
	lsl r20 ; Shift the whole conglomeration left a bit.
	rol r17
	rol r18
	rol r19
	clr r20
	rol r20 ; Now r20:19:18:17 holds FN_CCL >> 7.
	ret

;
; FAT_getclsector_load
;
;  Load X with SD_BUF + offset for cluster pointer.
;
;  Input:
;    r22: Low byte of cluster # (FAT_getclsector sets this).
;
;  Output:
;    r27:r26 (X): Pointer to cluster.
;
;  Side effects:
;    r22, r23 trashed.
;
FAT_getclsector_load:
	; Load X with the target.
	andi r22, 0x7F ; This is the same as r22 % 128, r22 is LSB of FN_CCL.
	clr r23 ; * 4, fat entries are 4 boits.
	lsl r22
	;rol r23 ; Unnecessary, note & 0x7F earlier.
	lsl r22
	rol r23
	ldi r27, high(SD_BUF) ; Read the actual pointer.
	ldi r26, low(SD_BUF)
	add r26, r22
	adc r27, r23
	ret

;
; FAT_addafat
;
;  Add FAT_AFAT to X
;
;  Input:
;    r20:r19:r18:r17: Number to add FAT_AFAT to.
;
;  Output:
;    r20:r19:r18:r17: Same + FAT_AFAT.
;
;  Side effects:
;    r16 trashed.
;
;  Notes:
;    See: FAT_getclsector
;
FAT_addafat:
	lds r16, FAT_AFAT + 3
	add r17, r16
	lds r16, FAT_AFAT + 2
	adc r18, r16
	lds r16, FAT_AFAT + 1
	adc r19, r16
	lds r16, FAT_AFAT + 0
	adc r20, r16
	ret

;
; FAT_wantcluster
;
;  Make sure the appropriate cluster is in FN_CCL, the current one might not exist.
;    If a cluster already exists (FN_FCL != 0) just return.
;    This is for the situation we just seeked to a cluster boundary.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r21, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;
;  Notes:
;    This doesn't work for creating the first cluster.
;
FAT_wantcluster:
	; Bail out if a cluster exists.
	adiw r31:r30, FN_CCL
	ld r16, Z
	sbiw r31:r30, FN_CCL
	andi r16, 0x80
	breq FAT_wantcluster_end
	; Ask for a free cluster.
	push r23 ; We store these because FAT_write cares.
	push r22
	call FAT_nextcluster
	pop r22
	pop r23
FAT_wantcluster_end:
	ret ; FAT_nextcluster error passthrough.

;
; FAT_firstcluster
;
;  Create the first cluster of a file (i.e. writing to an empty file).
;  If a cluster already exists (FN_FCL != 0) just return.
;
;  Input:
;    Z (r31:r30): File handle.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r21, r22, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;
FAT_firstcluster:
	; Bail out if a cluster exists.
	adiw r31:r30, FN_FCL
	ld r20, Z+
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	sbiw r31:r30, FN_FCL + 4
	clr r16
	cp r17, r16
	cpc r18, r16
	cpc r19, r16
	cpc r20, r16
	clc
	brne FAT_firstcluster_fail
	; Ask for a free cluster.
	call FAT_getfree ; Preserve Y, Z and r22.
	brcs FAT_firstcluster_fail
	call FAT_getfree_load ; r20:..:r19 now will hold new sector.
	; Save it to FN_FCL, FAT_CCL.
	adiw r31:r30, FN_FCL
	ldi r16, 2
		st Z+, r20 
		st Z+, r19
		st Z+, r18
		st Z+, r17
		dec r16 
	brne -6 ; FNORDER FN_FCL and FN_CCL sequential.
	; Load DFSE without destroying r21.
	sbiw r31:r30, (FN_FCL + 8) ; FNORDER Get file node start.
	push r21 ; From FAT_getfree.
	call FAT_getdent
	pop r21
	brcs FAT_firstcluster_fail
	; Get the cluster back and write it to X.
	call FAT_getfree_load ; Preserve X.
	adiw r27:r26, 0x14 ; High word of cluster.
	st X+, r19 ; Little endian madness.
	st X+, r20
	adiw r27:r26, 4 ; Skip over time.
	st X+, r17 ; Low word of cluster.
	st X+, r18
	; Get DFSE adress back and write it.
	ld r20, Z+ ; FNORDER Want FN_DFSE.
	ld r19, Z+
	ld r18, Z+
	ld r17, Z+
	sbiw r31:r30, FN_DFSE + 4 ; Restore Z.
	call SD_write
FAT_firstcluster_fail:
	ret ; SD_write error passthrough.

;
; FAT_getfree
;
;  Allocate a new cluster in the FAT.
;  Used by FAT_nextcluster and FAT_write only.
;
;  Input:
;    -
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;    r21: Cluster index in the FAT_CFS sector.
;    FAT_CFS (SRAM): The sector the cluster is in.
;      Thus: FAT_CFS * 128 + r21 = The cluster #.
;      See FAT_getfree_load, it can translate this.
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r22, r24, r25, SD_BUF (SRAM), r27:r26 (X) trashed.
;
FAT_getfree:
	call FAT_getfree_real
	brcc FAT_getfree_ok
	cpi r16, ERR_FSF
	brne FAT_getfree_ok
	call FAT_getfree_real ; First ERR_FSF can be a lie.
FAT_getfree_ok:
	ret
FAT_getfree_real:
	; Get "current fat sector".
	lds r17, FAT_CFS + 3
	lds r18, FAT_CFS + 2
	lds r19, FAT_CFS + 1
	lds r20, FAT_CFS + 0
	; Add FAT_AFAT.
	call FAT_addafat
	; Read that shit.
	call SD_read
	brcc +1
		ret
	; Search for a free cluster (0).
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
	clr r21 ; Loop counter.
	clr r17 ; We'll use this to compare against.
FAT_getfree_loop0:
	ld r18, X+ ; These are small endians.
	cp r18, r17
	ld r18, X+
	cpc r18, r17
	ld r18, X+
	cpc r18, r17
	ld r18, X+
	brne FAT_getfree_loop0_continue ; We oughta ignore upper bits but andi affects carry.
	andi r18, 0x0F
	breq FAT_getfree_yes
FAT_getfree_loop0_continue:
	inc r21
	cpi r21, 128 ; 128 clusters in a fat table sector.
	brne FAT_getfree_loop0
	; Increment FAT_CFS, compare with FAT_SPF and retry.
	lds r17, FAT_CFS + 3
	lds r18, FAT_CFS + 2
	lds r19, FAT_CFS + 1
	lds r20, FAT_CFS + 0
	ldi r16, 1
	add r17, r16
	clr r16
	adc r18, r16
	adc r19, r16
	adc r20, r16
	sts FAT_CFS + 3, r17
	sts FAT_CFS + 2, r18
	sts FAT_CFS + 1, r19
	sts FAT_CFS + 0, r20
	lds r16, FAT_SPF + 3
	cp r17, r16
	lds r16, FAT_SPF + 2
	cpc r18, r16
	lds r16, FAT_SPF + 1
	cpc r19, r16
	lds r16, FAT_SPF + 0
	cpc r20, r16
	breq +1 ; Too far.
		rjmp FAT_getfree_real
	; Here means nothing found. Reset FAT_CFS and return error.
	clr r16
	sts FAT_CFS + 3, r16
	sts FAT_CFS + 2, r16
	sts FAT_CFS + 1, r16
	sts FAT_CFS + 0, r16
	ldi r16, ERR_FSF
	sec
	ret
FAT_getfree_yes:
	; This means we got a free cluster at FAT_CFS + r21 * 4 (in the FAT).
	; It actually is cluster # FAT_CFS * 128 + r21
	; Also X points to right after the FAT entry.
	ldi r17, 0x0F ; Allocate that sector as EOF.
	st -X, r17
	ldi r17, 0xFF
	st -X, r17
	st -X, r17
	st -X, r17
	; Store the edited FAT sector.
	lds r17, FAT_CFS + 3
	lds r18, FAT_CFS + 2
	lds r19, FAT_CFS + 1
	lds r20, FAT_CFS + 0
	call FAT_tablewrite ; This doesn't touch r21.
	ret ; Errors from SD_write passed through.

;
; FAT_getfree_load
;
;  Translate the FAT_CFS + r21 returned by FAT_getfree into a cluster number.
;
;  Input:
;    r21 and FAT_CFS unchanged from FAT_getfree.
;
;  Output:
;    r20:r19:r18:r17: The cluster.
;
;  Side effects:
;    Only flags trashed really.
;
FAT_getfree_load:
	lds r20, FAT_CFS + 3 ; Load FAT_CFS * 128.
	lds r17, FAT_CFS + 2 ; For why, see description of FAT_getfree.
	lds r18, FAT_CFS + 1
	lds r19, FAT_CFS + 0
	lsl r20
	rol r17
	rol r18
	rol r19
	clr r20
	rol r20
	add r17, r21 ; Add r21, also see FAT_getfree.
	clr r16
	adc r18, r16
	adc r19, r16
	adc r20, r16
	ret

;
; FAT_killcluster
;
;  Free a cluster, and load the next one to memory.
;
;  Input:
;    r20:r19:r18:r17: The cluster to free, rotated 8 bits right.
;      This is for FAT_getclsector2.
;    T flat: Set means write last cluster mark.
;
;  Output:
;    r20:r19:r18:r17: The next cluster (0x0FFFFFFF on EOF), already stripped of upper 4 bits.
;      In the same rotated format.
;    C flag set on error, r16 loaded with errno.
;
;  Side effects:
;    r0 (only bit 0), r16, r21, r22, r23, r24, r25, r27:r26 (X) trashed.
;
;  Notes:
;    Highly recommend looking at FAT_getclsector code to understand usage.
;
FAT_killcluster:
	; Get the sector that holds the next cluster pointer.
	call FAT_getclsector2
	push r20 ; Store this sector so we can write it back.
	push r19
	push r18
	push r17
	call FAT_addafat
	call SD_read
	brcs FAT_killcluster_fail
	call FAT_getclsector_load
	; Get back the sector.
	pop r17
	pop r18
	pop r19
	pop r20
	; Store next cluster in r24:..:r21.
	ld r21, X+
	ld r22, X+
	ld r23, X+
	ld r24, X+
	; Remove cluster pointer from FAT.
	brtc FAT_killcluster_noeof
	ldi r16, 0x0F ; Write last cluster marker.
	st -X, r16
	ldi r16, 0xFF
	st -X, r16
	st -X, r16
	st -X, r16
	rjmp FAT_killcluster_eofdone
FAT_killcluster_noeof:
	clr r16 ; Free cluster.
	st -X, r16
	st -X, r16
	st -X, r16
	st -X, r16
FAT_killcluster_eofdone:
	; Write to FATs, and load the pointer.
	push r24 ; Note that r21 and r23 actually don't get trashed.
	push r23 ; NOTE We could optimize this, but be careful of _fail.
	push r22
	push r21
	call FAT_tablewrite
	brcs FAT_killcluster_fail
	pop r20 ; Put the pointer in the correct registers for FAT_getclsector2.
	pop r17
	pop r18
	pop r19
	andi r19, 0x0F
	clc
	ret
FAT_killcluster_fail:
	pop r16 ; Restore stack and return.
	pop r16
	pop r16
	pop r16
	ret

;
; FAT_tablewrite
;
;  Write a sector of the FAT. This takes FAT mirroring into account.
;
;  Input:
;    r20:r19:r18:r17: The sector number (inside FAT).
;    SD_BUF (SRAM): What to write.
;
;  Output:
;    C flag set on error (and r16 loaded with errno).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r22, r24, r25, r27:r26 (X) trashed.
;
;  Notes:
;    We always write all FATs, since it's easier.
;
FAT_tablewrite:
	; Add FAT_START.
	lds r16, FAT_START + 3
	add r17, r16
	lds r16, FAT_START + 2
	adc r18, r16
	lds r16, FAT_START + 1
	adc r19, r16
	lds r16, FAT_START + 0
	adc r20, r16
	; Loop.
	lds r22, FAT_CPY
FAT_tablewrite_loop:
	; Write the sector in the current copy.
	push r20 ; Store position.
	push r19
	push r18
	push r17
	call SD_write
	pop r17
	pop r18
	pop r19
	pop r20
	brcs FAT_tablewrite_end ; Error check for SD_write.
	; Add FAT_SPF.
	lds r16, FAT_SPF + 3
	add r17, r16
	lds r16, FAT_SPF + 2
	adc r18, r16
	lds r16, FAT_SPF + 1
	adc r19, r16
	lds r16, FAT_SPF + 0
	adc r20, r16
	; Count down and loop.
	dec r22
	brne FAT_tablewrite_loop
	clc
FAT_tablewrite_end:
	ret

;
; FAT_clearsdbuf
;
;  Fill SD_BUF with zeros.
;
;  Input:
;    -
;
;  Output:
;    SD_BUF (SRAM): Guaranteed zeros.
;
;  Side effects:
;    r16, r27:r26 (X) trashed.
;
FAT_clearsdbuf:
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
	clr r16
FAT_clearsdbuf_loop: ; Fill a sector with zeros.
	st X+, r16
	cpi r26, low(SD_BUF + 512)
	brne FAT_clearsdbuf_loop
	cpi r27, high(SD_BUF + 512)
	brne FAT_clearsdbuf_loop
	ret
