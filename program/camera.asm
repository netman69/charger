; Notice that some cameras have more equalizing pulses than others.
;   If vsync_wait always renders the same field, the function might need editing.
;   See note in the comments of function.
; Both offsets below have a minimum of 1 (or they'll act as if 256).
.equ h_offset = 27 ; Aligned to center the analog video capture.
.equ v_offset = 12
.equ v_lines = 480 ; Must be greater than 256 and divisible by 2 for the code to work.

.equ v_dport      = PIND ; Digital input port used.
.equ v_dport_sync = 3    ; Pin # that gets sync pulses.
.equ v_dport_data = 2    ; Pin that gets "digitized" video.

;
; vsync_wait
;
;  Waits until vsync pulse train and reports field.
;
;  Input:
;    -
;
;  Output:
;    r20: 0 for first field, 1 for second field.
;
;  Side effects:
;    r16, r17, r21 trashed.
;
vsync_wait:
	in r16, v_dport ; TODO this can be optimized
	andi r16, 1 << v_dport_sync ; Is sync signal.
	breq -3
	clr r17
	in r16, v_dport
	inc r17
	andi r16, 1 << v_dport_sync
	brne -4 ; Falls through on sync pulse end.
	cpi r17, 30 ; Normal sync pulse counts to 14-15, the long ones are 85-86
	brlo vsync_wait
vsync_wait2:
	in r16, v_dport
	andi r16, 1 << v_dport_sync
	breq -3
	clr r17
	in r16, v_dport
	inc r17
	andi r16, 1 << v_dport_sync
	brne -4 ; Falls through on sync pulse end.
	cpi r17, 30 ; Normal sync pulse counts to 14-15, the long ones are 85-86
	brge vsync_wait2
	; Skip equalizing pulses.
	;   NOTE for some cameras one of these might need commenting.
	;   Or adding one...
	call sync_wait
	call sync_wait
	call sync_wait
	call sync_wait
	;call sync_wait
	; Check which field we have.
	clr r17
	in r16, v_dport
	inc r17 ; Counts up to 95 on first field, 197 on second.
	andi r16, 1 << v_dport_sync
	breq -4
	in r16, v_dport
	andi r16, 1 << v_dport_sync
	brne -3 ; Falls through on sync pulse end.
	; Set r16 to the field #.
	clr r20
	cpi r17, 145 ; In the middle of difference.
	brlo +1
	inc r20
	; Skip the blank lines.
	ldi r21, v_offset
vsync_wait_blank:
	call sync_wait
	dec r21
	brne vsync_wait_blank
	ret

;
; sync_wait
;
;  Waits until (any) sync pulse.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 and r17 trashed.
;
sync_wait:
	in r16, v_dport
	andi r16, 1 << v_dport_sync
	breq -3
	in r16, v_dport
	andi r16, 1 << v_dport_sync
	brne -3 ; Falls through on sync pulse end.
	ret
