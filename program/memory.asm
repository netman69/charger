;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; This holds constants for all the memory locations used. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.equ VIDEO_BUF = 0x100 ; 512 bytes, MUST BE ALIGNED to 256 byte boundary. Buffer to store data captured from camera.
.equ SD_BUF    = 0x300 ; 512 bytes. Buffer for SD card data read/written.
.equ SD_CFG    = 0x500 ; 5 bytes. Where to store configuration data for SD card.
                       ; 1 byte used for CCS, 4 reserved for card size.
.equ FAT_CFG   = 0x505 ; 32 bytes of globals for FAT config stuff. Only 23 used really.
.equ FAT_RFN   = 0x525 ; 24 bytes for the root file node. Only 22 used really.
.equ HTTPD_FN  = 0x53D ; 24 bytes for file node used by HTTPD.
.equ HTTPD_BUF = 0x555 ; 256 bytes buffer for HTTPD to store filenames etc. NOTE If tight on space could use SD_BUF.
.equ HTTPD_BUF_END = 0x655 ; End of HTTPD_BUF.
.equ TMP_BUF   = 0x655 ; 16 bytes or so. Where to store strings from BinToAsc etc.
; End is 0x8FF.
