;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SD card stuff.                                            ;;
;;                                                           ;;
;; This file needs memory.asm to assemble (described below). ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; Usage of this library:
; - Provide equ statements for:
;     SD_BUF: Place where we can read a sector to/from, 512 bytes.
;     SD_CFG: Storage for globals, only one byte right now.
;       For future proofing suggest reserving 5 bytes, so card size would fit.
; - Set SD_CS to the pin hooked to CS of card.
;
; The following functions are all one needs for basic use:
;   - SD_init: Initialize SD card, this needs to be done before SD_read/SD_write.
;   - SD_read: Read a block.
;   - SD_write: Write a block.
;
;  Read the comments before said functions to learn how to use them.
;  A 7bit and 16bit CRC implementation are also provided, and could serve their generic purpose.
;

; Notes about SD cards:
;   - On reset the first CMD0 is sometimes responded to but not heard.
;   - Even in SPI mode HC/XC cards want 8 clocks after an operation.
;   - It appears the most reliable way to enter SPI mode is first clock 80 times,
;       then pull CS low, clock a few times (I did 80) too, and follow above advice.
;   - On SDSC cards (CCS 0 or v1.x) adresses are in bytes and block size can be set.*
;   - On SDHC or XC cards size is in 512 byte steps and size can't be set.*
;   - Misaligned / cross block-boundary access on SDCS cards is an optional extra.*
;
;   * Note that this driver allows only 512 byte blocks and abstracts above said differences.
;

.equ SD_CS_DDR = DDRB ; DDR for CS.
.equ SD_CS_PORT = PORTB ; PORT for CS.
.equ SD_CS = 0 ; PORTB pin hooked to CS on card.

; On error of SD_init/SD_read/SD_write functions carry will be set.
; Also r16 will be loaded with the err no.
; Additional error numbers are defined in sd.asm.
.equ ERR_RD = 0x01 ; Errors over 0x80 are reserved for fat32.asm.
.equ ERR_WR = 0x02
.equ ERR_INI = 0x03 ; SD init error.
.equ ERR_CRC = 0x04

; Locations to store global variables, offset by SD_CFG.
.equ SD_CFG_CCS = SD_CFG + 0 ; "Card Capacity Status" 1 if HC or XC card. Adresses are 512 byte blocks instead of bytes if 1.

.equ SD_DEBUG = 0

;
; SD_init_port
;
;  Initialize CS line.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    -
;
SD_init_port:
	sbi SD_CS_DDR, SD_CS
	sbi SD_CS_PORT, SD_CS ; CS high.
	ret

;
; SD_init
;
;  Initialize ourselves as SPI master and put SD card in SPI mode.
;
;  Input:
;    -
;
;  Output:
;    C flag set on error.
;
;  Side effects:
;    r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r30 (ZL), r31 (ZH) trashed.
;    SPI clock settings modified.
;
SD_init:
	; r23 will hold some flags.
	;   bit 0: Cleared for v1.x card.
	ldi r23, 0b00000001
	; Set clock rate fck/64 (250kHz).
	in r16, SPCR ; TODO Can't cbi instruction do this?
	andi r16, ~(1 << SPR0)
	ori r16, (1 << SPR1)
	out SPCR, r16
	clr r16
	out SPSR, r16
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Make clear to the card we want to use SPI mode. ;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	sbi SD_CS_PORT, SD_CS ; CS high, for sure.
	; Clock 10 bytes, spec says we should give >= 74 clocks.
	ldi r16, 0xFF
	ldi r19, 10
SD_init_loop0:
	call SPI_send
	dec r19
	brne SD_init_loop0
	clr r21 ; CMD0 attempts countdown.
	; Select slave.
	cbi SD_CS_PORT, SD_CS
	; Give it some clocks so it can think.
	ldi r19, 10
SD_init_loop1:
	call SPI_send ; r16 still 0xFF
	dec r19
	brne SD_init_loop1
	; Send CMD0 until it's accepted.
	call SD_cmd_clarg
SD_cmd0:
	ldi r16, 0x40 | 0 ; Command or'd with 0x40.
	call SD_cmd_R1
	; Check if we're in idle mode yet.
	cpi r16, 1
	breq SD_cmd0_pass
	dec r21
	brne SD_cmd0
	; This is reached after 256 tries for CMD0
	rjmp SD_init_fail
SD_cmd0_pass:
	; Sometimes the first try is a lie, when resetting HC cards.
	ldi r16, 0x40 | 0 ; Command or'd with 0x40.
	call SD_cmd_R1 ; Arguments are still 0 from before.
	cpi r16, 1
	breq +1
	rjmp SD_init_fail
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; SPI mode entered successfully.         ;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	; Turn CRC checking on for ALL commands, since we are sending it.
	ldi r16, 0x40 | 59
	call SD_cmd_clarg
	ldi r17, 1
	call SD_cmd_R1
	cpi r16, 1
	breq +1
	rjmp SD_init_fail
	; Send CMD8.
	ldi r16, 0x40 | 8 ; Command or'd with 0x40.
	call SD_cmd_clarg
	ldi r18, 1 ; 2.7-3.6v
	ldi r17, 0xAA ; Recommended "check pattern".
	call SD_cmd_R3
	cpi r16, 5 ; Check if illegal command, if yes it's v1.x card.
	brne SD_init_ver2
	andi r23, 0xFE ; Bit 0 of r23 holds 1 if it's v2.x
	clr r16
	sts SD_CFG_CCS, r16
	rjmp SD_init_cmd58
SD_init_ver2:
	; Result should be:
	;   r16 = 1
	;   r18 & 0x0F = 1 (voltage)
	;   r17 = 0xAA (check pattern echo)
	cpi r16, 1
	breq +1
	rjmp SD_init_fail
	andi r18, 0x0F
	cpi r18, 1
	breq +1
	rjmp SD_init_fail
	cpi r17, 0xAA
	breq +1
	rjmp SD_init_fail
	; Get that bastard out of idle mode.
	clr r21 ; Idle timeout counter.
	clr r22 ; More counter, it can take long.
SD_init_cmd58:
	ldi r16, 0x40 | 58
	call SD_cmd_clarg
	ldi r17, 1
	call SD_cmd_R3
	cpi r16, 1
	breq +1
	rjmp SD_init_fail
	; Check if 2.8-3.6v is supported, most cards seem to say yes. This whole voltage window strikes me as odd.
	cpi r19, 0xFF ; Bit 7 of r20 is for 2.7-2.8v it also usually appears to be set.
	breq +1
	rjmp SD_init_fail
SD_init_idle:
	; Send CMD55 (prepare for acmd).
	ldi r16, 0x40 | 55 ; Command or'd with 0x40.
	call SD_cmd_clarg ; NOTE clearing the argument is optional, the card shouldn't care.
	call SD_cmd_R1
	andi r16, 0xFE
	breq +1
	rjmp SD_init_fail
	; Send ACMD41 (Initialize card).
	; The argument should be 0 for v1.x cards.
	; For 2.x cards bit 30 declares the host supports HC cards.
	ldi r16, 0x40 | 41 ; Command or'd with 0x40.
	call SD_cmd_clarg
	sbrc r23, 0 ; Skip if v1.x card.
	ldi r20, 1 << 6
	call SD_cmd_R1
	cpi r16, 0
	breq SD_init_ready
	dec r21
	brne SD_init_idle
	clr r21
	dec r22
	brne SD_init_idle
	rjmp SD_init_fail
SD_init_ready:
	; Send CMD58 (for CCS, not needed with v1.x card).
	sbrs r23, 0
	rjmp SD_init_noccs
	ldi r16, 0x40 | 58 ; Command or'd with 0x40.
	call SD_cmd_clarg
	call SD_cmd_R3
	cpi r16, 0
	breq +1
	rjmp SD_init_fail
	clr r16
	sbrc r20, 6	; CCS is r20 bit 6.
	inc r16
	sts SD_CFG_CCS, r16
SD_init_noccs:
	; At this point we're allowed to speed up.
	in r16, SPCR
	andi r16, ~((1 << SPR1) | (1 << SPR0)) ; Fosc/4, 5MHz.
	out SPCR, r16
	; Set block size to 512 (the only size supported by HC/XC).
	; We'll also find out if the new speed works.
	ldi r16, 0x40 | 16 ; Command or'd with 0x40.
	call SD_cmd_clarg
	ldi r18, 2
	call SD_cmd_R1
	cpi r16, 0
	breq +1
	rjmp SD_init_fail
	; Deselect slave.
	sbi SD_CS_PORT, SD_CS
	clc
	ret
SD_init_fail:
	sbi SD_CS_PORT, SD_CS
	ldi r16, ERR_INI
	sec
	ret

;
; SD_read
;
;  Read a block from the SD card.
;
;  Input:
;    r20:r19:r18:r17: Adress of data to read, in multiples of 512.
;    MSB         LSB
;
;  Output:
;    SRAM (SD_BUF): 512 bytes of data.
;    C flag set on error (and r16 loaded with err no).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r24, r25, r27:r26 (X) trashed.
;
SD_read:
	push r31
	push r30
	bld r0, 0
	; Retry up to 3 times if the CRC failed.
	rcall SD_read_noretry
	brtc SD_read_done ; T set on CRC error.
	rcall SD_read_noretry
	brtc SD_read_done
	rcall SD_read_noretry
	brtc SD_read_done
SD_read_done:
	bst r0, 0
	pop r30
	pop r31
	ret
SD_read_noretry:
	clt ; CRC error flips it on.
	; Enable card.
	cbi SD_CS_PORT, SD_CS
	; Check if it's SC, if so multiply adress by 512.
	lds r16, SD_CFG_CCS
	lsr r16
	brcs SD_read_notranslate
	mov r20, r19
	mov r19, r18
	mov r18, r17
	clr r17
	lsl r18
	rol r19
	rol r20
SD_read_notranslate:
	; Send the read command.
	ldi r16, 0x40 | 17 ; Command or'd with 0x40.
	call SD_cmd_R1
	cpi r16, 0
	brne SD_read_fail
	clr r17
SD_read_wait:
	call SPI_recv
	cpi r16, 0xFE
	breq SD_read_go
	andi r16, 0xF0 ; 0b0000xxxx is an error token.
	breq SD_read_fail
	dec r17
	brne SD_read_wait
	rjmp SD_read_fail
SD_read_go:
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
	clr r25
	clr r24
	rcall SD_read_step
	rcall SD_read_step
	; Store CRC in r19:r20.
	call SPI_recv
	mov r19, r16
	call SPI_recv
	mov r20, r16
	; Check the CRC.
	cp r19, r25
	brne SD_read_fail_crc
	cp r20, r24
	brne SD_read_fail_crc
	call SPI_recv ; Obligatory 8 clocks after CRC.
	sbi SD_CS_PORT, SD_CS
	clc
	ret
SD_read_fail_crc:
	set
SD_read_fail:
	call SPI_recv ; Obligatory 8 clocks after error or failed CRC, redundant for other errors.
	sbi SD_CS_PORT, SD_CS
	ldi r16, ERR_RD
	brtc +1
	ldi r16, ERR_CRC
	sec
	ret
SD_read_step:
	clr r17
SD_read_substep:
	call SPI_recv
	st X+, r16
	;call UART_sendbyte
	call crc16
	dec r17
	brne SD_read_substep
	ret

;
; SD_write
;
;  Write a block to the SD card.
;
;  Input:
;    r20:r19:r18:r17: Adress of data to write, in multiples of 512.
;    MSB         LSB
;    SRAM (SD_BUF): 512 bytes of data.
;
;  Output:
;    C flag set on error (and r16 loaded with err no).
;
;  Side effects:
;    r0 (only bit 0), r16, r17, r18, r19, r20, r24, r25, r27:r26 (X) trashed.
;
SD_write:
	push r31
	push r30
	bld r0, 0
	; Retry up to 3 times if the CRC failed.
	rcall SD_write_noretry
	brtc SD_write_done
	rcall SD_write_noretry
	brtc SD_write_done
	rcall SD_write_noretry
	brtc SD_write_done
SD_write_done:
	bst r0, 0
	pop r30
	pop r31
	ret
SD_write_noretry:
	clt ; CRC error flips it on.
	; Enable card.
	cbi SD_CS_PORT, SD_CS
	; Check if it's SC, if so multiply adress by 512.
	lds r16, SD_CFG_CCS
	lsr r16
	brcs SD_write_notranslate
	mov r20, r19
	mov r19, r18
	mov r18, r17
	clr r17
	lsl r18
	rol r19
	rol r20
SD_write_notranslate:
	; Send the write command.
	ldi r16, 0x40 | 24 ; Command or'd with 0x40.
	call SD_cmd_R1
	cpi r16, 0
	brne SD_write_fail
	clr r17
	; Send start char.
	ldi r16, 0xFE
	call SPI_send
	ldi r27, high(SD_BUF)
	ldi r26, low(SD_BUF)
	clr r25
	clr r24
	rcall SD_write_step
	rcall SD_write_step
	; Send CRC out.
	mov r16, r25
	call SPI_send
	mov r16, r24
	call SPI_send
	; Wait for cards response.
	clr r17
SD_write_waitresp:
	call SPI_recv
	andi r16, 0x1F
	cpi r16, 0x05
	breq SD_write_success
	cpi r16, 0x0B
	breq SD_write_fail_crc
	cpi r16, 0x0D
	breq SD_write_fail_busy	
	dec r17
	brne SD_write_waitresp
	rjmp SD_write_fail
SD_write_success:
	rcall SD_write_waitbusy
	call SPI_recv ; Clock 8 more times (not sure if required).
	sbi SD_CS_PORT, SD_CS
	clc
	ret
SD_write_fail_crc:
	set
SD_write_fail_busy:
	rcall SD_write_waitbusy
SD_write_fail:
	call SPI_recv ; Clock 8 more times (not sure if required).
	sbi SD_CS_PORT, SD_CS
	ldi r16, ERR_WR
	brtc +1
	ldi r16, ERR_CRC
	sec
	ret
SD_write_step:
	clr r17
SD_write_substep:
	ld r16, X+
	call SPI_send
	call crc16
	dec r17
	brne SD_write_substep
	ret
SD_write_waitbusy:
	clr r18 ; Writing takes longer than 256 cycles.
	clr r17
SD_write_waitbusy_loop:
	call SPI_recv
	cpi r16, 0xFF
	brne +1
	ret
	dec r18
	brne SD_write_waitbusy_loop
	dec r17
	brne SD_write_waitbusy_loop
	pop r16 ; "return"
	pop r16
	rjmp SD_write_fail

;
; SD_cmd_clarg
;
;  Clear r20-r17, named so because we use it for arguments to SD_cmd.
;
;  Input:
;    -
;
;  Output:
;    r17, r18, r19, r20: 0
;
;  Side effects:
;    -
;
SD_cmd_clarg:
	clr r20
	clr r19
	clr r18
	clr r17
	ret

;
; SD_cmd_R1
;
;  Send an SD command, receive R1 response.
;
;  Input:
;    r16: Command #, already or'd with 0x40.
;    r20-r17: Argument.
;
;  Output:
;    r16: Response.
;
;  Side effects:
;    r25, r30, r31 (Z) trashed
;
SD_cmd_R1:
	call SD_cmd
	push r16
	; We need to clock another 8bits for the card to be happy.
	ldi r16, 0xFF
	call SPI_send
	pop r16
.if SD_DEBUG
	call UART_sendbyte
.endif
	ret

;
; SD_cmd_R2
;
;  Send an SD command, receive R2 response.
;
;  Input:
;    r16: Command #, already or'd with 0x40.
;    r20-r17: Argument.
;
;  Output:
;    r16: Response.
;    r20: Response part 2.
;
;  Side effects:
;    r25, r30, r31 (Z) trashed
;
SD_cmd_R2:
	call SD_cmd
	push r16
	call SPI_recv
	mov r20, r16
	; We need to clock another 8bits for the card to be happy.
	ldi r16, 0xFF
	call SPI_send
.if SD_DEBUG
	pop r16
	push r16
	call UART_sendbyte
	mov r16, r20
	call UART_sendbyte
.endif
	pop r16
	ret

;
; SD_cmd_R3
;
;  Send an SD command, receive R3 or R7 response.
;
;  Input:
;    r16: Command #, already or'd with 0x40.
;    r20-r17: Argument.
;
;  Output:
;    r16: Response.
;    r20: Response part 2.
;    r19: Response part 3.
;    r18: Response part 4.
;    r17: Response part 5.
;
;  Side effects:
;    r25, r30, r31 (Z) trashed
;
SD_cmd_R3:
	call SD_cmd
	push r16
	call SPI_recv
	mov r20, r16
	call SPI_recv
	mov r19, r16
	call SPI_recv
	mov r18, r16
	call SPI_recv
	mov r17, r16
	; We need to clock another 8bits for the card to be happy.
	ldi r16, 0xFF
	call SPI_send
.if SD_DEBUG
	pop r16
	push r16
	call UART_sendbyte
	mov r16, r20
	call UART_sendbyte
	mov r16, r19
	call UART_sendbyte
	mov r16, r18
	call UART_sendbyte
	mov r16, r17
	call UART_sendbyte
.endif
	pop r16
	ret

;
; SD_cmd
;
;  Send an SD command.
;
;  Input:
;    r16: Command #, already or'd with 0x40.
;    r20-r17: Argument.
;
;  Output:
;    r16: First byte of response. 0xFF on failure.
;
;  Side effects:
;    r24, r25, r30, r31 (Z) trashed
;
SD_cmd:
	;cbi SD_CS_PORT, SD_CS
	clr r25 ; CRC.
	call SPI_send
	call crc7
	mov r16, r20
	call SPI_send
	call crc7
	mov r16, r19
	call SPI_send
	call crc7
	mov r16, r18
	call SPI_send
	call crc7
	mov r16, r17
	call SPI_send
	call crc7
	mov r16, r25 ; Checksum, shift left and put a 1.
	sec
	rol r16
	call SPI_send
SD_cmd_getresp:
	clr r24
	ldi r16, 0xFF ; Dummy command for receiving response.
SD_cmd_block:
	dec r24
	breq SD_cmd_fail
	call SPI_send
	in r16, SPDR
	sbrc r16, 7 ; Bit 7 is 0 for a response.
	rjmp SD_cmd_block
	;sbi SD_CS_PORT, SD_CS
	ret
SD_cmd_fail:
	;sbi SD_CS_PORT, SD_CS
	ldi r16, 0xFF
	ret

;
; crc7
;
;  Update 7-bit CRC.
;
;  Input:
;    r25: CRC.
;    r16: Data.
;
;  Output:
;    r25: Updated CRC.
;
;  Side effects:
;    r30 (ZL), r31 (ZH) trashed.
;
;  Notes:
;   Updating crc7 works as follows:
;    crc = crc7_table[(crc << 1) ^ data]
;
crc7:
	lsl r25
	eor r25, r16
	ldi r31, high(crc7_table << 1)
	ldi r30, low(crc7_table << 1)
	add r30, r25
	brcc +1 ; NOTE this could be optimized out if CRC table is aligned.
	inc r31
	lpm r25, Z
	ret

;
; crc16
;
;  Update 16-bit CCITT CRC.
;
;  Input:
;    r16: Byte.
;    r25:r24: CRC to be updated.
;
;  Output:
;    r25:r24: Updated 16-bit CCITT CRC.
;
;  Side effects:
;    r16, r20, r31:r30 (Z) trashed.
;
crc16:
	; crc = crctab[(crc >> 8 ^ data[i]) & 0XFF] ^ (crc << 8);
	ldi r31, high(crc16_table << 1)
	ldi r30, low(crc16_table << 1)
	eor r16, r25
	clr r20
	lsl r16
	rol r20
	add r30, r16
	adc r31, r20
	mov r16, r24
	lpm r24, Z+
	lpm r25, Z
	eor r25, r16
	ret

crc7_table:
	.db 0x00, 0x09, 0x12, 0x1b, 0x24, 0x2d, 0x36, 0x3f,
	.db 0x48, 0x41, 0x5a, 0x53, 0x6c, 0x65, 0x7e, 0x77,
	.db 0x19, 0x10, 0x0b, 0x02, 0x3d, 0x34, 0x2f, 0x26,
	.db 0x51, 0x58, 0x43, 0x4a, 0x75, 0x7c, 0x67, 0x6e,
	.db 0x32, 0x3b, 0x20, 0x29, 0x16, 0x1f, 0x04, 0x0d,
	.db 0x7a, 0x73, 0x68, 0x61, 0x5e, 0x57, 0x4c, 0x45,
	.db 0x2b, 0x22, 0x39, 0x30, 0x0f, 0x06, 0x1d, 0x14,
	.db 0x63, 0x6a, 0x71, 0x78, 0x47, 0x4e, 0x55, 0x5c,
	.db 0x64, 0x6d, 0x76, 0x7f, 0x40, 0x49, 0x52, 0x5b,
	.db 0x2c, 0x25, 0x3e, 0x37, 0x08, 0x01, 0x1a, 0x13,
	.db 0x7d, 0x74, 0x6f, 0x66, 0x59, 0x50, 0x4b, 0x42,
	.db 0x35, 0x3c, 0x27, 0x2e, 0x11, 0x18, 0x03, 0x0a,
	.db 0x56, 0x5f, 0x44, 0x4d, 0x72, 0x7b, 0x60, 0x69,
	.db 0x1e, 0x17, 0x0c, 0x05, 0x3a, 0x33, 0x28, 0x21,
	.db 0x4f, 0x46, 0x5d, 0x54, 0x6b, 0x62, 0x79, 0x70,
	.db 0x07, 0x0e, 0x15, 0x1c, 0x23, 0x2a, 0x31, 0x38,
	.db 0x41, 0x48, 0x53, 0x5a, 0x65, 0x6c, 0x77, 0x7e,
	.db 0x09, 0x00, 0x1b, 0x12, 0x2d, 0x24, 0x3f, 0x36,
	.db 0x58, 0x51, 0x4a, 0x43, 0x7c, 0x75, 0x6e, 0x67,
	.db 0x10, 0x19, 0x02, 0x0b, 0x34, 0x3d, 0x26, 0x2f,
	.db 0x73, 0x7a, 0x61, 0x68, 0x57, 0x5e, 0x45, 0x4c,
	.db 0x3b, 0x32, 0x29, 0x20, 0x1f, 0x16, 0x0d, 0x04,
	.db 0x6a, 0x63, 0x78, 0x71, 0x4e, 0x47, 0x5c, 0x55,
	.db 0x22, 0x2b, 0x30, 0x39, 0x06, 0x0f, 0x14, 0x1d,
	.db 0x25, 0x2c, 0x37, 0x3e, 0x01, 0x08, 0x13, 0x1a,
	.db 0x6d, 0x64, 0x7f, 0x76, 0x49, 0x40, 0x5b, 0x52,
	.db 0x3c, 0x35, 0x2e, 0x27, 0x18, 0x11, 0x0a, 0x03,
	.db 0x74, 0x7d, 0x66, 0x6f, 0x50, 0x59, 0x42, 0x4b,
	.db 0x17, 0x1e, 0x05, 0x0c, 0x33, 0x3a, 0x21, 0x28,
	.db 0x5f, 0x56, 0x4d, 0x44, 0x7b, 0x72, 0x69, 0x60,
	.db 0x0e, 0x07, 0x1c, 0x15, 0x2a, 0x23, 0x38, 0x31,
	.db 0x46, 0x4f, 0x54, 0x5d, 0x62, 0x6b, 0x70, 0x79

crc16_table:
	.dw 0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
	.dw 0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
	.dw 0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
	.dw 0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
	.dw 0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
	.dw 0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
	.dw 0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
	.dw 0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
	.dw 0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
	.dw 0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
	.dw 0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
	.dw 0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
	.dw 0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
	.dw 0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
	.dw 0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
	.dw 0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
	.dw 0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
	.dw 0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
	.dw 0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
	.dw 0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
	.dw 0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
	.dw 0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	.dw 0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
	.dw 0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
	.dw 0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
	.dw 0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
	.dw 0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
	.dw 0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
	.dw 0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
	.dw 0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
	.dw 0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
	.dw 0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
