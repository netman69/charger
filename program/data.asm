str_hello: .db "MAT Li-Ion Charging platform v0.01 beta.", 0x0D, 0x0A, 0, 0
str_ma: .db "mA", 0x0D, 0x0A, 0, 0
str_mv: .db "mV", 0x0D, 0x0A, 0, 0
str_nl: .db 0x0D, 0x0A, 0, 0
str_clr: .db 0x1B, '[', '2', 'J', 0, 0
str_home: .db 0x1B, '[', 'H', 0
str_sd_fail: .db "SD card init failed.", 0x0D, 0x0A, 0, 0
str_sd_fail2: .db "SD card access failed.", 0x0D, 0x0A, 0, 0
