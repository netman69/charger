.equ RTC_ADDR = 0xD0 ; 7-bit adress must be left aligned.

;
; RTC_read
;
;  Read ds1307 RTC to Z as "YYYY-MM-DD hh:mm:ss".
;
;  Input:
;    Z (r31:r30): Adress to store the string (19 bytes).
;
;  Output:
;    C flag set on error.
;
;  Side effects:
;    r16, r17, r18 trashed.
;
RTC_read:
	; Send start.
	call TWI_start
	brcc +1
	rjmp RTC_read_error
	ldi r16, RTC_ADDR
	call TWI_sendbyte
	brcc +1
	rjmp RTC_read_error
	; Write address, 0.
	ldi r16, 0
	call TWI_sendbyte
	brcc +1
	rjmp RTC_read_error
	; Send repeated start for read.
	call TWI_start
	brcc +1
	rjmp RTC_read_error
	ldi r16, RTC_ADDR + 1 ; Bit 0 = 1 means read.
	call TWI_sendbyte
	brcc +1
	rjmp RTC_read_error
	; Read the time and date.
	adiw r31:r30, 19 ; We write in reverse like YYYY-MM-DD hh:mm:ss.
	ldi r18, '0' ; We will use this repeatedly.
	; Seconds.
	call TWI_recvbyte
	brcc +1
	rjmp RTC_read_error
	mov r17, r16
	swap r16
	andi r16, 0x0F
	add r16, r18
	andi r17, 0x0F
	add r17, r18
	st -Z, r17
	st -Z, r16
	ldi r16, ':'
	st -Z, r16
	; Minutes.
	call TWI_recvbyte
	brcc +1
	rjmp RTC_read_error
	mov r17, r16
	swap r16
	andi r16, 0x0F
	add r16, r18
	andi r17, 0x0F
	add r17, r18
	st -Z, r17
	st -Z, r16
	ldi r16, ':'
	st -Z, r16
	; Hours.
	call TWI_recvbyte
	brcc +1
	rjmp RTC_read_error
	mov r17, r16
	swap r16
	andi r16, 0x0F
	add r16, r18
	andi r17, 0x0F
	add r17, r18
	st -Z, r17
	st -Z, r16
	ldi r16, ' '
	st -Z, r16
	; Day (1-7=MTWTFSS).
	call TWI_recvbyte
	brcc +1
	rjmp RTC_read_error
	; Day of month.
	call TWI_recvbyte
	brcc +1
	rjmp RTC_read_error
	mov r17, r16
	swap r16
	andi r16, 0x0F
	add r16, r18
	andi r17, 0x0F
	add r17, r18
	st -Z, r17
	st -Z, r16
	ldi r16, '-'
	st -Z, r16
	; Month.
	call TWI_recvbyte
	brcc +1
	rjmp RTC_read_error
	mov r17, r16
	swap r16
	andi r16, 0x0F
	add r16, r18
	andi r17, 0x0F
	add r17, r18
	st -Z, r17
	st -Z, r16
	ldi r16, '-'
	st -Z, r16
	; Year.
	call TWI_recvbyte_nak
	brcc +1
	rjmp RTC_read_error
	mov r17, r16
	swap r16
	andi r16, 0x0F
	add r16, r18
	andi r17, 0x0F
	add r17, r18
	st -Z, r17
	st -Z, r16
	ldi r16, '0' ; TODO Change this after 2100 unless we changed RTC by then.
	st -Z, r16
	ldi r16, '2'
	st -Z, r16
	call TWI_stop
	clc
	ret
RTC_read_error:
	; TODO figure out if need stop.
	sec
	ret

RTC_set:
	; TODO
	ret