echo off
setlocal
set file=main

tools\gavrasm %file%.asm
if not %ERRORLEVEL% == 0 (
    goto fail
)

tools\avrdude -c arduino -P com4 -b 57600 -p ATMega328P -vv -U flash:w:%file%.hex
if not %ERRORLEVEL% == 0 (
    goto fail
)

pause
goto end

:fail
echo *** ERROR occured ***
pause

:end