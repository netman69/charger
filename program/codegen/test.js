function code(x) {
	var par = ((((x >> 7) + (x >> 6) + 1) & 1) << 5) | // bits 6 and 7 (odd)
	          ((((x >> 5) + (x >> 4)) & 1) << 4) | // bits 5 and 4 (even)
	          ((((x >> 3) + (x >> 2) + 1) & 1) << 3) | // bits 3 and 2 (odd)
	          ((((x >> 1) + (x >> 0)) & 1) << 2) | // bits 1 and 0 (even)
	          ((((x >> 7) + (x >> 4) + (x >> 3) + (x >> 0) + 1) & 1) << 1) | // (odd)
	          (((x >> 6) + (x >> 5) + (x >> 2) + (x >> 1)) & 1); // (even)
	return x + ((par) << 8);
}

/*
x = code(123);
var f = WScript.CreateObject("Scripting.Filesystemobject").CreateTextFile("generated.xpm", 2);
f.Write("! XPM2\n42 40 2 1\n1 c #FFFFFF\n0 c #000000\n");
for (var i = 0; i < 14; ++i) {
//	f.Write("0");
	f.Write((x & (1 << 14 >> i)) ? "0" : "1");
//	f.Write("1");
}
f.Close();
*/
WScript.Echo("code = 0x" + code(123).toString(16));
