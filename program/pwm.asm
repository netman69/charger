;
; PWM_init
;
;  Sets up timer 0 and 2 for fast PWM mode with 4 channels
;   and enables the relevant outputs.
;  Frequency is around 7.8kHz.
;  Slight offset between timer0 and timer1.
;  After this the duty cycle of the follwoing pins is controlled by:
;    D6: OCR0A
;    D5: OCR0B
;    D9: OCR1AH:OCR1AL (10bit)
;    D10: OCR1BH:OCR1BL (10bit)
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
PWM_init:
	call PWM_init0
	call PWM_init2
	ret

;
; PWM_init0
;
;  Equal to above but only enables timer 0 (D5 and D6).
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
PWM_init0:
	; Set up timer0 for fast pwm, outputs on D5 and D6.
	ldi r16, 0
	out OCR0A, r16 ; Duty 0 (D6)
	out OCR0B, r16 ; Duty 1 (D5)
	ldi r16, (1 << COM0A1) | (1 << COM0B1) | (1 << WGM01) | (1 << WGM00)
	out TCCR0A, r16
	ldi r16, (1 << CS01)
	out TCCR0B, r16
	; Enable the relevant outputs.
	in r16, DDRD
	ori r16, (1 << 5) | (1 << 6)
	out DDRD, r16
	; The end.
	ret

;
; PWM_init2
;
;  Equal to above but only enables timer 1 (D9 and D10).
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
PWM_init2:
	; Set up timer1 for fast pwm, outputs on D9 and D10.
	ldi r16, 0
	sts OCR1AL, r16 ; Duty 2 (D9)
	sts OCR1AH, r16
	sts OCR1BL, r16 ; Duty 3 (D10)
	sts OCR1BH, r16
	ldi r16, (1 << COM2A1) | (1 << COM2B1) | (1 << WGM12) | (1 << WGM11) | (1 << WGM10)
	sts TCCR1A, r16
	ldi r16, (1 << CS10)
	sts TCCR1B, r16
	; Enable the relevant outputs.
	in r16, DDRB
	ori r16, (1 << 1) | (1 << 2)
	out DDRB, r16
	; The end.
	ret
