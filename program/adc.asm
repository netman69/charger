;
; ADC_init
;
;  Initialise the ADC how we want it.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
ADC_init:
	ldi r16, (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0)
	sts ADCSRA, r16
	ldi r16, (1 << REFS0) 
	sts ADMUX, r16
	ret

;
; ADC_read
;
;  Read an ADC channel.
;
;  Input:
;    r16: The channel.
;
;  Output:
;    ADCL, ADCH: Result.
;
;  Side effects:
;    r16 and 17 trashed.
;
ADC_read:
	lds r17, ADMUX
	andi r17, 0xF0
	or r17, r16
	sts ADMUX, r17
	lds r17, ADCSRA
	ori r17, 1 << ADSC
	sts ADCSRA, r17
ADC_read_block:
	lds r17, ADCSRA
	andi r17, 1 << ADSC
	brne ADC_read_block
	ret
