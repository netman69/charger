;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HTTP server stuff.                                        ;;
;;                                                           ;;
;; This file needs memory.asm to assemble (described below). ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
; We want a space in SRAM to store received GET requests and filenames.
;   HTTPD_BUF should be equ'd as that.
;   HTTPD_BUF_END should be equ'd to it's end.
;   256 bytes is a reasonable size, perhaps even 128 if space is tight.
;     14 bytes are taken for the request.
;     26 bytes is enough for 1-folder deep filenames.
;     11 bytes are required for decoding filenames.
;     Therefore 14 + 26 + 11 = 51 bytes is the absolute minimum.
;

; We use the end to store filenames temporarily (max 11 bytes, dos short names).
.equ HTTPD_BUF_END_REAL = HTTPD_BUF_END - 11 ; This will be our end of buffer for practical purposes.
.equ HTTPD_FNAME = HTTPD_BUF_END_REAL ; Optionally we could use TMP_BUF instead if interrupts won't mess with it.

;
; HTTPD_init
;
;  Initialize HTTP server, WNet chip will listen to port 80, on socket 0.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16, r17, r25:r24, r29 trashed.
;
HTTPD_init:
	; Lissen to port 80.
	ldi r19, 0
	ldi r18, 80
	ldi r28, 0 ; Socket 0.
	call WNet_listen
	ret

;
; HTTPD_process
;
;  Check if there is a HTTP client, if so respond to it.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r0, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r27:r26 (X), r29:r28 (Y), r31:r30 (Z) trashed.
;
;  Notes:
;    - Only short filenames are supported (also for HTTPD).
;    - Filenames written as 11 sequential characters without a dot also count.
;    - Characters after filename will be ignored.
;
HTTPD_process:
	; Quit if no client is there.
	call WNet_getstatus
	cpi r16, WNET_Sn_SR_ESTABLISHED
	breq +1
		ret
	; Receive data.
	;   We attempt to receive until either:
	;   - r27:r26 comes to 0 (This puts a time constraint on things).
	;   - HTTPD_BUF full.
	;   - Newline received.
	ldi r31, high(HTTPD_BUF) ; Destination.
	ldi r30, low(HTTPD_BUF)
	ldi r27, 0 ; Try 65536 times.
HTTPD_process_recvloop:
	ldi r26, 0
	ldi r28, 0 ; Socket 0 (inside the loop cause later we trash it).
	call WNet_getreceived
	; Set available space to whichever is smallest, buffer space available or received size.
	ldi r16, low(HTTPD_BUF_END_REAL)
	ldi r17, high(HTTPD_BUF_END_REAL)
	sub r16, r30
	sbc r17, r31
	cp r16, r18
	cpc r17, r19
	brcc HTTPD_process_recvloop_gotspace
	mov r18, r16
	mov r19, r17
HTTPD_process_recvloop_gotspace:
	; Check if we're actually trying to receive something.
	clr r16
	cp r18, r16
	cpc r19, r16
	breq HTTPD_process_recvloop_continue
	; Store current point and receive data.
	push r31
	push r30
	call WNet_recv
	; Check if we have newline.
	pop r28
	pop r29
HTTPD_process_recvloop_findnl:
	ld r16, Y+
	cpi r16, 0x0A
	breq HTTPD_process_recvloop_ok
	cp r30, r28
	cpc r31, r29
	brcc HTTPD_process_recvloop_findnl
	; Check if we still have space.
	ldi r16, low(HTTPD_BUF_END_REAL)
	ldi r17, high(HTTPD_BUF_END_REAL)
	cp r30, r16
	cpc r31, r17
	brne +1
	rjmp HTTPD_process_400
	; Check if we got tired of trying, loop if not.
HTTPD_process_recvloop_continue:
	sbiw r27:r26, 1
	brne HTTPD_process_recvloop
	; Bail out, we have no newline yet.
	rjmp HTTPD_process_400
HTTPD_process_recvloop_ok:
	; Check if actual get request.
	ldi r31, high(HTTPD_BUF)
	ldi r30, low(HTTPD_BUF)
	ld r16, Z+
	cpi r16, 'G'
	brne HTTPD_process_notget
	ld r16, Z+
	cpi r16, 'E'
	brne HTTPD_process_notget
	ld r16, Z+
	cpi r16, 'T'
	brne HTTPD_process_notget
	ld r16, Z+
	cpi r16, ' '
	brne HTTPD_process_notget
	ld r16, Z+
	cpi r16, '/'
	brne HTTPD_process_notget
	rjmp +1
HTTPD_process_notget:
		rjmp HTTPD_process_400
	; Translate filename.
	ldi r16, high(FAT_RFN) ; r3:r2 hold file node adress.
	mov r3, r16
	ldi r16, low(FAT_RFN)
	mov r2, r16
HTTPD_process_folder:
	ldi r27, high(HTTPD_FNAME)
	ldi r26, low(HTTPD_FNAME)
	call HTTPD_translatefilename
	sbiw r31:r30, 1
	ld r16, Z+
	cpi r16, '/'
	brne HTTPD_process_gotfile
	; Open folder and look in it.
	push r31
	push r30
	mov r31, r3
	mov r30, r2
	ldi r29, high(HTTPD_FNAME)
	ldi r28, low(HTTPD_FNAME)
	ldi r27, high(HTTPD_FN)
	ldi r26, low(HTTPD_FN)
	call FAT_open ; Replaces r31:r30 with opened folder.
	mov r3, r31
	mov r2, r30
	pop r30
	pop r31
	brcc +1 ; Check for error from FAT_open.
		rjmp HTTPD_process_404
	rjmp HTTPD_process_folder
HTTPD_process_gotfile:
	; Look for file.
	mov r31, r3
	mov r30, r2
	ldi r29, high(HTTPD_FNAME)
	ldi r28, low(HTTPD_FNAME)
	ldi r27, high(HTTPD_FN)
	ldi r26, low(HTTPD_FN)
	call FAT_open
	brcc +1
		rjmp HTTPD_process_404
	; Send start.
	ldi r31, high(str_httpd_ok_start << 1)
	ldi r30, low(str_httpd_ok_start << 1)
	ldi r19, high(str_httpd_ok_start_len)
	ldi r18, low(str_httpd_ok_start_len)
	ldi r28, 0 ; Socket 0.
	call WNet_send_pmem
	; Send size.
	ldi r31, high(HTTPD_FN)
	ldi r30, low(HTTPD_FN)
	call FAT_getlength
	ldi r31, high(HTTPD_BUF)
	ldi r30, low(HTTPD_BUF)
	call BinToAsc32
	clr r19
	ldi r18, 10
HTTPD_process_trim0: ; Trim zeros.
	ld r16, Z
	cpi r16, '0'
	brne HTTPD_process_sendlen
	dec r18
	brne +2 ; If 0 then we need 1 digit.
		inc r18
		rjmp HTTPD_process_sendlen
	adiw r31:r30, 1
	rjmp HTTPD_process_trim0
HTTPD_process_sendlen:
	call WNet_send_sram
	; Send type.
	ldi r31, high(str_httpd_ok_bin << 1)
	ldi r30, low(str_httpd_ok_bin << 1)
	ldi r19, high(str_httpd_ok_bin_len)
	ldi r18, low(str_httpd_ok_bin_len)
	ldi r27, high(HTTPD_FNAME + 8)
	ldi r26, low(HTTPD_FNAME + 8)
	ld r16, X+
	cpi r16, 'H'
	brne HTTPD_process_nothtm
	ld r16, X+
	cpi r16, 'T'
	brne HTTPD_process_nothtm
	ld r16, X+
	cpi r16, 'M'
	brne HTTPD_process_nothtm
	ldi r31, high(str_httpd_ok_html << 1)
	ldi r30, low(str_httpd_ok_html << 1)
	ldi r19, high(str_httpd_ok_html_len)
	ldi r18, low(str_httpd_ok_html_len)
HTTPD_process_nothtm:
	ldi r28, 0 ; Socket 0.
	call WNet_send_pmem
HTTPD_process_sendloop: ; TODO enable/disable interrupts smartly. For SD_BUF usage etc.
	; Load file node.
	ldi r31, high(HTTPD_FN)
	ldi r30, low(HTTPD_FN)
	; Check if 0-length.
	ldi r23, 2
	clr r22
	call FAT_seekmax
	clr r16
	cp r22, r16
	cpc r23, r16
	breq HTTPD_process_end
	; Read it.
	call FAT_read_sector
	brcs HTTPD_process_end ; NOTE This is an error, but what to do?
	; Send that shit.
	ldi r23, 2
	clr r22
	call FAT_seekmax
	mov r19, r23
	mov r18, r22
	ldi r31, high(SD_BUF)
	ldi r30, low(SD_BUF)
	ldi r28, 0 ; Socket 0.
	call WNet_send_sram
	; Check how far we can seek and do that.
	ldi r23, 2
	clr r22
	call FAT_seekmax
	clr r16 ; Stop at end.
	cp r22, r16
	cpc r23, r16
	brne +1
		rjmp HTTPD_process_end
	mov r17, r23
	mov r16, r22
	ldi r31, high(HTTPD_FN)
	ldi r30, low(HTTPD_FN)
	call FAT_seek
	brcs HTTPD_process_end ; NOTE This is an error, but what to do?
	; Check if client is still connected, continue if so.
	ldi r28, 0 ; Socket 0.
	call WNet_getstatus
	cpi r16, WNET_Sn_SR_ESTABLISHED
	breq HTTPD_process_sendloop ; Loop.
	; We're already disconnected, re-initialize.
	call HTTPD_init
	ret
HTTPD_process_end:
	; Make sure we're asking for the right socket.
	ldi r28, 0
	; Disconnect.
	call WNet_disconnect
	; Re-initialize.
	call HTTPD_init
	ret
HTTPD_process_404:
	; Send 404 error.
	ldi r31, high(str_httpd404 << 1)
	ldi r30, low(str_httpd404 << 1)
	ldi r19, high(str_httpd404_len)
	ldi r18, low(str_httpd404_len)
	ldi r28, 0 ; Socket 0.
	call WNet_send_pmem
	rjmp HTTPD_process_end
HTTPD_process_400:
	; Send 404 error.
	ldi r31, high(str_httpd400 << 1)
	ldi r30, low(str_httpd400 << 1)
	ldi r19, high(str_httpd400_len)
	ldi r18, low(str_httpd400_len)
	ldi r28, 0 ; Socket 0.
	call WNet_send_pmem
	rjmp HTTPD_process_end

;
; HTTPD_translatefilename
;
;  Translate a filename for the filesystem.
;
;  Input:
;    Z (r31:r30): Point to original filename, I.E. "test.txt".
;      Terminated by space (0x20) or '/'.
;    X (r27:r26): Point to 11 bytes for DOS filename, I.E. "TEST    TXT".
;
;  Output:
;    -
;
;  Side effects:
;    r16, r17, X (r27:r26), Z (r31:r30) trashed.
;
HTTPD_translatefilename:
	ldi r17, 11
HTTPD_translatefilename_continue:
	; Load a character.
	ld r16, Z+
	; Check if there's a dot.
	cpi r16, '.'
	breq HTTPD_translatefilename_ext
	; Check if this is the end.
	cpi r16, ' '
	breq HTTPD_translatefilename_done
	cpi r16, '/'
	breq HTTPD_translatefilename_done
	; Capitalize it.
	cpi r16, 'a'
	brcs HTTPD_translatefilename_notletter
	cpi r16, 'z'
	brcc HTTPD_translatefilename_notletter
	andi r16, ~0x20
HTTPD_translatefilename_notletter:
	st X+, r16
	dec r17
	brne HTTPD_translatefilename_continue
	rjmp HTTPD_translatefilename_done
HTTPD_translatefilename_ext:
	; Add spaces until extension.
	ldi r16, ' '
HTTPD_translatefilename_spadd:
	cpi r17, 4
	brcs HTTPD_translatefilename_spdone
	st X+, r16
	dec r17
	rjmp HTTPD_translatefilename_spadd
HTTPD_translatefilename_spdone:
	rjmp HTTPD_translatefilename_continue
HTTPD_translatefilename_done:
	; Pad with spaces still.
	ldi r16, ' '
	st X+, r16
	dec r17
	brne HTTPD_translatefilename_done
	ret

str_httpd400:
	.db "HTTP/1.0 400 Bad Request"
	.db 0x0A, "Content-Length: 178", 0x0A, "Content-Type: text/html", 0x0A, 0x0A, "<!DOCTYPE html><html><head><title>400 Bad Request</title></head><body><h1>Bad Request</h1><p>The request was not understood.</p></body></html>", 0x0A, 0 ; 208 characters.
	.equ str_httpd400_len = 213

str_httpd404:
	.db "HTTP/1.0 404 Not Found"
	.db 0x0A, "Content-Length: 140", 0x0A, "Content-Type: text/html", 0x0A, 0x0A, "<!DOCTYPE html><html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL was not found.</p></body></html>", 0x0A ; 208 characters.
	.equ str_httpd404_len = 208

str_httpd_ok_start:
	.db "HTTP/1.0 200 OK", 0x0A, "Content-Length: "
	.equ str_httpd_ok_start_len = 32

str_httpd_ok_html:
	.db 0x0A, "Content-Type: text/html", 0x0A, 0x0A
	.equ str_httpd_ok_html_len = 26

str_httpd_ok_bin:
	.db 0x0A, "Content-Type: application/octet-stream", 0x0A, 0x0A
	.equ str_httpd_ok_bin_len = 41
