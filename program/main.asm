.device ATMega328P
.equ F_CPU = 16000000
.org 0

.equ current_offset = 0
.equ current_factor = 0x829C

.equ voltage_offset = 6449
.equ voltage_factor = 0xCBB6

start:
	; Set up stack pointer to be end of ram (RAMEND).
	ldi r16, low(RAMEND)
	out SPL, r16 ; stack pointer low
	ldi r16, high(RAMEND)
	out SPH, r16 ; stack pointer high

	; We will use green arduino led as sign we've started.
.if 0 ; LED is same pin as SPI CLK.
	ldi r16, 1 << 5 ; set bit5 of r16
	out DDRB, r16 ; turn port B pin 5 to output
	out PORTB, r16 ; turn it on, arduino nano v3 clone led lights
.endif

	; Initialize our shit.
	call UART_init
	call PWM_init
	call ADC_init

	; Set up the PWM outputs.
	ldi r16, 186 ; 4.2v
	out OCR0A, r16 ; Duty 0 (D6)
	ldi r16, 10 ; low-ish current
	out OCR0B, r16 ; Duty 1 (D5)
	
	ldi r16, 0
	sts OCR1AH, r16
	ldi r16, 0xFF
	sts OCR1AL, r16
	ldi r16, 2
	sts OCR1BH, r16
	ldi r16, 0
	sts OCR1BL, r16


rjmp wnet_test
;rjmp rtctest
;rjmp fattest
;rjmp sdtest
;rjmp video_test
;rjmp lintestb
;rjmp lintestd
;rjmp lintestc

.include "tests.asm"

.include "memory.asm"
.include "data.asm"
.include "uart.asm"	
.include "spi.asm"
.include "twi.asm"
.include "pwm.asm"
.include "adc.asm"
.include "util.asm"
.include "camera.asm"
.include "barcode.asm"
.include "rtc.asm"
.include "sd.asm"
.include "fat32.asm"
.include "w5100.asm"
.include "dhcp.asm"
.include "httpd.asm"
