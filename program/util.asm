;
; delay
;
;  Wastes time.
;
;  Input:
;    r17: How much time to waste on a scale of 0 to 255.
;
;  Output:
;    -
;
;  Side effects:
;    r17, 18 and 19 trashed (0).
;
delay: ; load r16 with length to wait
	delay_loop3: ldi r18, 255
	delay_loop2: ldi r19, 255
	delay_loop1:
		dec r19
		brne delay_loop1
		dec r18
		brne delay_loop2
		dec r17
		brne delay_loop3
	ret

;
; BinToAsc
;
;  Convert 16bit number to ASCII
;
;  Input:
;    r16: Low byte.
;    r17: High byte.
;    Z: Safe location to store 5 bytes without overflowing ZH (ZL < 250).
;
;  Output:
;    Z: Restored to beginning of number.
;    R17, 18 and 19: Still hold the BCD data.
;
;  Side effects:
;    r20 and 21 trashed.
;
BinToAsc:
	call BinToBCD
	ldi r21, '0'
	mov r20, r19
	swap r20
	andi r20, 0x0F
	add r20, r21
	st Z+, r20
	mov r20, r19
	andi r20, 0x0F
	add r20, r21
	st Z+, r20
	mov r20, r18
	swap r20
	andi r20, 0x0F
	add r20, r21
	st Z+, r20
	mov r20, r18
	andi r20, 0x0F
	add r20, r21
	st Z+, r20
	mov r20, r17
	swap r20
	andi r20, 0x0F
	add r20, r21
	st Z+, r20
	clr r20
	st Z+, r20
	subi r30, 6
	ret

;
; BinToBCD
;
;  Convert 16bit to BCD
;
;  Input:
;    r16: Low byte.
;    r17: High byte.
;
;  Output:
;    R17, 18 and 19: Hold the BCD data.
;
;  Side effects:
;    r20 and 21 trashed.
;
BinToBCD: ; Mat double dabble. r16=lo r17=hi
	clr r18
	clr r19
	; Shaft one, too -free.
	cpi r17, 0xA0
	brcs +3
	ldi r20, 0x60
	add r17, r20
	adc r18, r19
	ldi r20, 0x30
	; Shaft fower, the stars align.
	cpi r17, 0x50
	brcs +1
	add r17, r20 ; carry impossibru
	; Shift five.
	lsl r16
	rol r17
	rol r18
	cpi r17, 0x50
	brcs +1
	add r17, r20 ; r20 still holds 0x30
	; Shaft sex.
	lsl r16
	rol r17
	rol r18
	cpi r17, 0x50
	brcs +1
	add r17, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	; Seven bottles on the wall.
	lsl r16
	rol r17
	rol r18
	cpi r17, 0x50
	brcs +1
	add r17, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	; A whole bite.
	lsl r16
	rol r17
	rol r18 ; Note that a bit might have slipped into the upper nibble if the first step overflowed.
	cpi r17, 0x50
	brcs +1
	add r17, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	; Nein!
	lsl r16
	rol r17
	rol r18
	cpi r17, 0x50
	brcs +1
	add r17, r20
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	; Tien met extra nibbels!
	lsl r16
	rol r17
	rol r18
	rol r19
	cpi r17, 0x50
	brcs +1
	add r17, r20
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	; Elf.
	lsl r16
	rol r17
	rol r18
	rol r19
	cpi r17, 0x50
	brcs +1
	add r17, r20
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	; Twaalf.
	lsl r16
	rol r17
	rol r18
	rol r19
	cpi r17, 0x50
	brcs +1
	add r17, r20
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	swap r19 ; we could eliminate this swap
	cpi r19, 0x50
	brcs +1
	add r19, r20
	swap r19 ; end of that swap we can eliminate
	; Thirteen returns.
	lsl r17
	rol r18
	rol r19
	cpi r17, 0x50
	brcs +1
	add r17, r20
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	swap r19
	cpi r19, 0x50
	brcs +1
	add r19, r20
	swap r19
	; 14 is repeat
	lsl r17
	rol r18
	rol r19
	cpi r17, 0x50
	brcs +1
	add r17, r20
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	swap r19
	cpi r19, 0x50
	brcs +1
	add r19, r20
	swap r19
	; This too.
	lsl r17
	rol r18
	rol r19
	cpi r17, 0x50
	brcs +1
	add r17, r20
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	cpi r18, 0x50
	brcs +1
	add r18, r20
	swap r18
	swap r19
	cpi r19, 0x50
	brcs +1
	add r19, r20
	swap r19
	; 16 is just shift
	lsl r17
	rol r18
	rol r19
	; The end.
	ret

;
; BinToAsc32
;
;  Convert 32bit integer to ASCII representation.
;
;  Input:
;    r20:r19:r18:r17: The number.
;    Z (r31:r30): location to store the number (10 digits).
;
;  Output:
;    11 bytes in SRAM pointed to by Z.
;
;  Side effects:
;    r16, r17, r18, r19, r20 trashed.
;
;  Notes:
;    Original author of this function is Peter Dannegger (danni@specs.de).
;    Original function gives output in registers.
;
BinToAsc32:
	ldi     r16, -1 + '0'
BinToAsc32_dec1:
	inc     r16
	subi    r18, 0xCA ; byte2(1000000000) ; -1000,000,000 until overflow
	sbci    r19, 0x9A ; byte3(1000000000)
	sbci    r20, 0x3B ; byte4(1000000000)
	brcc    BinToAsc32_dec1
	st      Z+, r16

	ldi     r16, 10 + '0'
BinToAsc32_dec2:
	dec     r16
	subi    r18, 0x1F ; +100,000,000 until no overflow
	sbci    r19, 0x0A ; In hex because gavrasm otherwise fails.
	sbci    r20, 0xFA ; 0xFA0A1F00 = -100000000
	brcs    BinToAsc32_dec2
	st      Z+, r16

	ldi     r16, -1 + '0'
BinToAsc32_dec3:
	inc     r16
	subi    r17, 0x80 ; byte1(10000000) ; -10,000,000
	sbci    r18, 0x96 ; byte2(10000000)
	sbci    r19, 0x98 ; byte3(10000000)
	sbci    r20, 0
	brcc    BinToAsc32_dec3
	st      Z+, r16

	ldi     r16, 10 + '0'
BinToAsc32_dec4:
	dec     r16
	subi    r17, 0xC0 ; +1,000,000
	sbci    r18, 0xBD ; 0xFFF0BDC0 = -1000000
	sbci    r19, 0xF0
	brcs    BinToAsc32_dec4
	st      Z+, r16

	ldi     r16, -1 + '0'
BinToAsc32_dec5:
	inc     r16
	subi    r17, 0xA0 ; byte1(100000) ; -100,000
	sbci    r18, 0x86 ; byte2(100000)
	sbci    r19, 0x01 ; byte3(100000)
	brcc    BinToAsc32_dec5
	st      Z+, r16

	ldi     r16, 10 + '0'
BinToAsc32_dec6:
	dec     r16
	subi    r17, 0xF0 ; +10,000
	sbci    r18, 0xD8 ; 0xFFFFD8F0 = -10000
	sbci    r19, 0xFF
	brcs    BinToAsc32_dec6
	st      Z+, r16

	ldi     r16, -1 + '0'
BinToAsc32_dec7:
	inc     r16
	subi    r17, 0xE8 ; byte1(1000) ; -1000
	sbci    r18, 0x03 ; byte2(1000)
	brcc    BinToAsc32_dec7
	st      Z+, r16

	ldi     r16, 10 + '0'
BinToAsc32_dec8:
	dec     r16
	subi    r17, 0x9C ; +100
	sbci    r18, 0xFF ; 0xFF9C = -100
	brcs    BinToAsc32_dec8
	st      Z+, r16

	ldi     r16, -1 + '0'
BinToAsc32_dec9: 
	inc     r16
	subi    r17, 10 ;-10
	brcc    BinToAsc32_dec9
	st      Z+, r16

	subi    r17, -10 - '0'
	st      Z+, r17
	;clr     r16
	;st      Z+, 0
	sbiw    r31:r30, 10 ; Restore Z.
	ret
