;;;;;;;;;;;;;;;;
;; I2C driver ;;
;;;;;;;;;;;;;;;;
;
; This has all stuff for handling the hardware TWI.
; A useful resource was:
;   https://aylo6061.com/2014/04/23/atmega328p-assembly-i2c-transactions/
;

;
; TWI_init
;
;  Set up I2C as master, 100kHz.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
TWI_init:
	; Set baud rate.
	ldi r16, 72
	sts TWBR, r16
	ldi r16, 0
	sts TWSR, r16
	ret

;
; TWI_start
;
;  Start I2C transaction.
;
;  Input:
;    -
;
;  Output:
;    C flag set on error.
;
;  Side effects:
;    r16 trashed.
;
TWI_start:
	; Send start condition.
	ldi r16, (1<<TWINT) | (1<<TWSTA) | (1<<TWEN)
	sts TWCR, r16
	call TWI_wait ; Wait for start sent.
	; Check if that worked.
	lds r16, TWSR
	andi r16, 0xF8
	cpi r16, 0x08 ; "start condition"
	breq TWI_start_ok
	cpi r16, 0x10 ; "restart condition"
	brne TWI_start_error
TWI_start_ok:
	clc
	ret
TWI_start_error:
	sec
	ret

;
; TWI_stop
;
;  Stop I2C transaction.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
TWI_stop:
	; Send stop thing.
	ldi r16, (1<<TWINT) | (1<<TWEN) | (1<<TWSTO)
	sts TWCR, r16
TWI_stop_wait: ; Wait for stop sent.
	lds r16, TWCR
	andi r16, 0x10 ; Check to see that no transmission is going on.
	brne TWI_stop_wait
	ret

;
; TWI_sendbyte
;
;  Send byte per I2C.
;
;  Input:
;    r16: The byte to send.
;
;  Output:
;    C flag set on error.
;
;  Side effects:
;    r16 trashed.
;
TWI_sendbyte:
	sts TWDR, r16
	ldi r16, (1<<TWINT) | (1<<TWEN)
	sts TWCR, r16
	call TWI_wait ; Wait until sent.
	; See if got ACK.
	lds r16, TWSR
	cpi r16, 0x18 ; SLA+W gives 0x18 for ACK (+ prescaler bits).
	breq TWI_sendbyte_ok
	cpi r16, 0x40 ; SLA+R gives 0x40.
	breq TWI_sendbyte_ok
	cpi r16, 0x28 ; Sending data gives 0x28 on ACK.
	brne TWI_sendbyte_error
TWI_sendbyte_ok:
	clc
	ret
TWI_sendbyte_error:
	sec
	ret

;
; TWI_recvbyte, TWI_recvbyte_nak
;
;  Stop I2C transaction.
;    _nak version sends NAK.
;
;  Input:
;    -
;
;  Output:
;    r16: The byte received.
;    C flag set on error.
;
;  Side effects:
;    r16 trashed.
;
TWI_recvbyte_nak:
	ldi r16, (1<<TWINT) | (1<<TWEN)
	rjmp TWI_recvbyte_real
TWI_recvbyte:
	ldi r16, (1<<TWINT) | (1<<TWEN) | (1<<TWEA)
TWI_recvbyte_real:
	sts TWCR, r16
	call TWI_wait ; Wait until received.
	; See if got ACK.
	lds r16, TWSR
	andi r16, 0xF0 ; 0x50 means received and ACK sent, 0x58 received and NAK sent.
	cpi r16, 0x50
	brne TWI_recvbyte_error
	lds r16, TWDR
	clc
	ret
TWI_recvbyte_error:
	sec
	ret

;
; TWI_wait
;
;  Returns when TWINT set.
;
;  Input:
;    -
;
;  Output:
;    -
;
;  Side effects:
;    r16 trashed.
;
TWI_wait:
	lds r16, TWCR
	sbrs r16, TWINT
	rjmp TWI_wait
	ret
