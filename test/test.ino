int cc_out = 5;
int cv_out = 6;
int i_in = 0;
int v_in = 1;

void setup() {
  TCCR0B = TCCR0B & 0b11111000 | 0x02;
  pinMode(cc_out, OUTPUT);
  pinMode(cv_out, OUTPUT);
  //analogWrite(cc_out, 227); // 1.0A
  analogWrite(cc_out, 124); // 0.501A
  //analogWrite(cc_out, 62);
  //analogWrite(cc_out, 252); // 1.0A
  //analogWrite(cv_out, 222); // 4.991v
  analogWrite(cv_out, 186); // 4.197v
  //analogWrite(cv_out, 88); // 2v
  //analogWrite(cv_out, 0);
  Serial.begin(9600);
  ADCSRA |= bit (ADPS0) | bit (ADPS1) | bit (ADPS2);
}

void loop() {
  int i = analogRead(i_in);
  int v = analogRead(v_in);
  unsigned long ri = i * 500L / 490L;
  long rv = (5609000L - (v - 132L) * 6366L) / 1000L;
  // 6449 - 6.366 * v
  Serial.println("\n\n---");
  Serial.println(i);
  Serial.println(ri);
  Serial.println("---");
  Serial.println(v);
  Serial.println(rv);
  delay(1000); // timer is extra fast cause TCCR0B settings
}

